import 'dart:convert';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import "package:flutter/material.dart";
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sicar/models/evaluation_network.dart';
import 'package:sicar/models/response_difficulte.dart';
import 'package:sicar/pages/phone_authentification.dart';

import 'database/controller_db.dart';
import 'global_config.dart';
import 'models/cvas.dart';
import 'models/difficulte.dart';
import 'models/langues.dart';
import 'models/module.dart';
import 'models/partenaire.dart';
import 'models/relais.dart';


Future isInternet() async {
  try {
    var url = Uri.http('google.com', '');
    final response = await http.get(
      url,
      headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    );
    print(url);
    print(response.body);
    return response.statusCode;
  } catch (e) {
    throw (e);
  }
}

Future<int> login(Relais relais) async {
  var url = Uri.http('${Configuration.base_url}','api/relais');
  //final String url = '${Configuration.base_url}relais';
  final client = http.Client();
  print(relais.toJson());
  final response = await http.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(relais.toJson()),
  );

  print(url);
  print(json.encode(relais.toJson()));

  print(response.statusCode);
  print(response.body);

  if(response.statusCode == 201){
    print("L'utilisateur a bien été crée");
  }
  return json.decode(response.body)["relais_id"];
}


Future<int> updateUser(Relais relais) async {
  var url = Uri.http('${Configuration.base_url}','api/relais');
  //final String url = '${Configuration.base_url}relais';
  final client = http.Client();
  print(relais.toJson());
  final response = await http.put(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(relais.toJson()),
  );

  print(url);
  print(json.encode(relais.toJson()));

  print(response.statusCode);
  print(response.body);

  if(response.statusCode == 201){
    print("L'utilisateur a bien été mise à jour");
  }
  return json.decode(response.body)["relais_id"];
}


Future<bool> sendEvaluation(EvaluationNetwork evaluationNetwork) async {
  var url = Uri.http('${Configuration.base_url}','api/evaluations');
  //final String url = '${Configuration.base_url}evaluations';
  final client = http.Client();
  print(evaluationNetwork.toJson());
  final response = await http.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(evaluationNetwork.toJson()),
  );

  print(url);
  print(json.encode(evaluationNetwork.toJson()));

  print(response.statusCode);
  print(response.body);

  if(response.statusCode == 201){
    print("L'évaluation est envoyée");
    return true;
  } else {
    return false;
  }
}



Future<List<Partenaire>> getPartenaire() async {
  var url = Uri.http('${Configuration.base_url}','api/partenaires');
  //final String url = '${Configuration.base_url}partenaires';
  //final client = http.Client();
  final response = await http.get(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );

  print(url);
  List<dynamic> body = json.decode(response.body);
  List<Partenaire> partenaire = [];


  print(response.body);
  if(response.statusCode == 200){
    body.forEach((element) {
      partenaire.add(Partenaire.fromJson(element)) ;
    });

  }
  return partenaire;
}


Future<List<Relais>> getRelais() async {
  var url = Uri.http('${Configuration.base_url}','api/relais');
  //final String url = '${Configuration.base_url}relais';
  final client = http.Client();
  final response = await http.get(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );


  List<dynamic> body = json.decode(response.body);
  List<Relais> relais = [];

  if(response.statusCode == 200){
    body.forEach((element) {
      relais.add(Relais.fromJson(element)) ;
    });

  }
  return relais;
}

Future<Relais> verifyPhone(String phone) async {
  var url = Uri.http('${Configuration.base_url}','phone');
  //final String url = '${Configuration.base_url}phone';
  final client = http.Client();
  final response = await http.post(
      url,
      headers: {HttpHeaders.contentTypeHeader: 'application/json'},
      body: phone
  );

  print(url);
  print(response.body);
  print(response.statusCode);

  var body = json.decode(response.body);
  Relais relais ;

  if(response.statusCode == 200){
    relais = Relais.fromJson(body);
  } else
    return relais;
}

Future<List<Langues>> getLangues() async {
  var url = Uri.http('${Configuration.base_url}','api/languages');
  //final String url = '${Configuration.base_url}languages';
  final client = http.Client();
  final response = await http.get(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );

  print(url);

  List<dynamic> body = json.decode(response.body);
  List<Langues> langues = [];


  print(response.statusCode);
  print(response.body);

  if(response.statusCode == 200){
    body.forEach((element) {
      langues.add(Langues.fromJson(element)) ;
    });

  }
  return langues;
}


Future<List<Cvas>> getCvas() async {
  var url = Uri.http('${Configuration.base_url}','api/cvas');
  //final String url = '${Configuration.base_url}cvas';
  final client = http.Client();
  final response = await http.get(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );

  print(url);

  List<dynamic> body = json.decode(response.body);
  List<Cvas> cvas = [];


  print(response.statusCode);
  print(response.body);

  if(response.statusCode == 200){
    body.forEach((element) {
      cvas.add(Cvas.fromJson(element)) ;
    });

  }
  return cvas;
}


Future<List<ReponseDifficulte>> getResponseDifficute() async {
  int userId = await getIdRelais();
  var url = Uri.http('${Configuration.base_url}','api/reponses/$userId');
  //final String url = '${Configuration.base_url}reponses/$userId';
  final client = http.Client();
  final response = await http.get(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );

  print(url);

  List<dynamic> body = json.decode(response.body);
  List<ReponseDifficulte> reponses = [];


  print(response.statusCode);
  print(response.body);

  if(response.statusCode == 200){
    body.forEach((element) {
      reponses.add(ReponseDifficulte.fromJson(element)) ;
    });

  }
  return reponses;
}

Future<List<ReponseDifficulte>> getResponselist() async {
  int userId = await getIdRelais();
  var url = Uri.http('${Configuration.base_url}','api/reponseslist/$userId');
  //final String url = '${Configuration.base_url}reponses/$userId';
  final client = http.Client();
  final response = await http.get(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );

  print(url);

  List<dynamic> body = json.decode(response.body);
  List<ReponseDifficulte> reponses = [];


  print(response.statusCode);
  print(response.body);

  if(response.statusCode == 200){
    body.forEach((element) {
      reponses.add(ReponseDifficulte.fromJson(element)) ;
    });

  }
  return reponses;
}

Future<List<ReponseDifficulte>> check_read() async {
  int userId = await getIdRelais();
  var url = Uri.http('${Configuration.base_url}','api/check_read/$userId');
  //final String url = '${Configuration.base_url}reponses/$userId';
  final client = http.Client();
  final response = await http.get(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );

  print(url);

  List<dynamic> body = json.decode(response.body);
  List<ReponseDifficulte> reponses = [];

  print(response.statusCode);
  print(response.body);

  if(response.statusCode == 200){
    print("Modification effectuée avec succes");

  }
  else
    {
      print("Echec de modificcation");
    }
  //return reponses;
}

Future<List<Difficulte>> getDifficute(int difficultesId) async {

  var url = Uri.http('${Configuration.base_url}','api/difficultes/$difficultesId');
  //final String url = '${Configuration.base_url}reponses/$userId';
  final client = http.Client();
  final response = await http.get(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );

  print(url);

  List<dynamic> body = json.decode(response.body);
  List<Difficulte> reponses = [];

  print(response.statusCode);
  print(response.body);

  if(response.statusCode == 200){
    body.forEach((element) {
      reponses.add(Difficulte.fromJson(element)) ;
    });
  }
  return reponses;
}


Future<List<Module>> getModule(int langue_id) async {
  var url = Uri.http('${Configuration.base_url}','api/modules-api');
  //final String url = '${Configuration.base_url}modules-api';
  final client = http.Client();

  final response = await http.post(
      url,
      headers: {HttpHeaders.contentTypeHeader: 'application/json'},
      body: json.encode({
        "language_id": langue_id
      })
  );

  print(url);

  List<dynamic> body = json.decode(response.body);
  List<Module> modules = [];


  print(response.statusCode);
  print(response.body);

  if(response.statusCode == 200){
    body.forEach((element) {
      modules.add(Module.fromJson(element)) ;
    });
   /* modules.forEach((element) {
      UserCtr().insertModule(element);
    });*/

  }
  return modules;
}

Future<List<Module>> getModule_(int cva_id, int langue_id) async {
  var url = Uri.http('${Configuration.base_url}','api/modules-api');
  //final String url = '${Configuration.base_url}modules-api';
  final client = http.Client();

  final response = await http.post(
      url,
      headers: {HttpHeaders.contentTypeHeader: 'application/json'},
      body: json.encode({
        "cva_id": cva_id,
        "language_id": langue_id
      })
  );

  print(url);

  List<dynamic> body = json.decode(response.body);
  List<Module> modules = [];


  print(response.statusCode);
  print(response.body);

  if(response.statusCode == 200){
    body.forEach((element) {
      modules.add(Module.fromJson(element)) ;
    });
    modules.forEach((element) {
      UserCtr().insertModule(element);
    });

  }
  return modules;
}


Future<int> sendDifficuty(Difficulte difficulte) async {
  var url = Uri.http('${Configuration.base_url}','api/difficultes');
  //final String url = '${Configuration.base_url}difficultes';
  final client = http.Client();
  print(difficulte.toJson());
  final response = await http.post(
      url,
      headers: {HttpHeaders.contentTypeHeader: 'application/json'},
      body: json.encode(difficulte.toJson())
  );

  print(url);


  print(response.statusCode);
  print(response.body);

  if(response.statusCode == 201){

    print("Envoie de la difficulté");
  }
  return response.statusCode;
}


Future<void> storeIdLangue(int id) async {
  if(id!=null) {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt("langueId", id);
  }
}

Future<int> getIdLangue() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  int id;
  if(prefs.containsKey("langueId"))
    id = prefs.getInt("langueId");

  return id;
}

Future<int> getIdRelais() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  int id;
  if(prefs.containsKey("user_id"))
    id = prefs.getInt("user_id");

  return id;
}


void setCurrentUser(Relais users) async {
  if (users != null) {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('current_user', json.encode(users));
  }
}


Future<Relais> getCurrentUser() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  Relais user = Relais();
//  prefs.clear();
  if (prefs.containsKey('current_user')) {
    print("prefs.containsKey('current_user_2')");
    print(json.decode(await prefs.get('current_user')));

    user = Relais.fromJson(json.decode(await prefs.get('current_user')));
    print(user.partenaire_id);
    print("Ici");
  }else{
    print("No user");
  }
  print(user);
  return user;
}

void setLoadingModule(List<String> moduleValue) async {
  if (moduleValue != null) {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setStringList('module_value', moduleValue);
  }
}

Future<String> getLoadingModule(String value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  List<String> list = prefs.getStringList("module_value");

  return list[int.tryParse(value)];
}

Future<void> logout(BuildContext context) async{
  final _auth = FirebaseAuth.instance;
  SharedPreferences preferences = await SharedPreferences.getInstance();
  await _auth.signOut();
  preferences.remove("user_id");
  Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => PhoneAuthentification()));
}