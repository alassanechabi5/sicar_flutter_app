
import '../components/constants_path.dart';

class Helper {


  static List<String> getLanguage(){
    return [
      PathImagesAudios.ZERMA,
      PathImagesAudios.MORE,
      PathImagesAudios.HAOUSSA,
      PathImagesAudios.GOURMENTCHE,
      PathImagesAudios.FRENCH,
      PathImagesAudios.FONGBE,
      PathImagesAudios.ENGLISH,
      PathImagesAudios.BIALI
    ];
  }





  static List<String> getCvaNom(){
    return [
      PathImagesAudios.APICULTURECVA,
      PathImagesAudios.BAOBABCVA,
      //PathImagesAudios.KARITECVA,
      PathImagesAudios.MUNGBEANCVA,
      PathImagesAudios.SESAMECVA,
      PathImagesAudios.SOJACVA,
    ];
  }

  static List<String>getFormationCVA(String cvaName) {
   if(cvaName == PathImagesAudios.APICULTURECVA)
     return getFormationApiculture();
   else if(cvaName == PathImagesAudios.BAOBABCVA)
     return getFormationBaobab();
   else if(cvaName == PathImagesAudios.MUNGBEANCVA)
     return getFormationMungBean();
   else if(cvaName == PathImagesAudios.SESAMECVA)
     return getFormationSesame();
   else
     return getFormationSodja();

  }

  static List<String>getCvaFormationSelectVideos(String cvaName) {
    if(cvaName == PathImagesAudios.APICULTURECVA)
      return videosApiculture;
    else if(cvaName == PathImagesAudios.BAOBABCVA)
      return videosBaobab;
    else if(cvaName == PathImagesAudios.MUNGBEANCVA)
      return videosMungBean;
    else if(cvaName == PathImagesAudios.SESAMECVA)
      return videosSesame;
    else
      return videosSodja;

  }
   static List<String> getFormationApiculture(){
    return [
      "lib/images/APICULTURE/img1.jpeg",
      "lib/images/APICULTURE/img2.jpg",
      "lib/images/semencesMaraîchères.jpg",
      "lib/images/neemBio.png",
      "lib/images/paastignamb.jpg"
    ];
  }

  static List<String> videosApiculture = [
  "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
  ];

   static List<String> getFormationBaobab(){
    return [
      "lib/images/neemBio.png",
      "lib/images/paastignamb.jpg"
    ];
  }

  static List<String> videosBaobab = [
    "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
  ];

  static List<String> getFormationMungBean(){
    return [
      "lib/images/semencesMaraîchères.jpg",
      "lib/images/APICULTURE/img1.jpeg",
      "lib/images/APICULTURE/img2.jpg",
    ];
  }

  static List<String> videosMungBean = [
    "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4",
  ];
  static List<String> getFormationSesame(){
    return [
      "lib/images/APICULTURE/img1.jpeg",
    ];
  }

  static List<String> videosSesame = [
    "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4"
  ];

  static List<String> getFormationSodja(){
    return [
      "lib/images/semencesMaraîchères.jpg",
      "lib/images/neemBio.png",
      "lib/images/APICULTURE/img1.jpeg",
    ];
  }

  static List<String> videosSodja= [
    "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/TearsOfSteel.mp4",
    "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/VolkswagenGTIReview.mp4",
  ];


  static List<String> getImagesCVA(){
    return [
      PathImagesAudios.APICULTURE,
      PathImagesAudios.BAOBAB,
      //PathImagesAudios.KARITE,
      PathImagesAudios.MUNGBEAN,
      PathImagesAudios.SESAME,
      PathImagesAudios.SODJA
    ];
  }
}