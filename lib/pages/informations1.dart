import 'package:country_code_picker/country_code_picker.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sicar/components/hexadecimal.dart';
import 'package:sicar/components/responsive_calcul.dart';
import 'package:sicar/components/submit_button.dart';
import 'package:sicar/components/text_classe.dart';
import 'package:sicar/database/controller_db.dart';
import 'package:sicar/models/relais.dart';
import 'package:sicar/pages/home_screen.dart';
import 'package:sicar/user_repository.dart' as userRepo;
import 'package:sicar/helper/helper_partenaire.dart' as partenaireHelp;




class Informations1 extends StatefulWidget {
  String phone;
  String language;
  int id;

  Informations1({this.phone, this.language, this.id});
  @override
  _Informations1State createState() => _Informations1State();
}

class _Informations1State extends State<Informations1> {
  Relais relais =Relais();



  final _scaffoldKey = GlobalKey<ScaffoldState>();
  List<RadioModelWhatsapp> sampleDataWhatsapp =List<RadioModelWhatsapp>();
  String sexe;
  String ethnie;
  String commune;
  TextEditingController nomController = TextEditingController();
  TextEditingController prenomsController = TextEditingController();
  TextEditingController numArrondissementController = TextEditingController();
  TextEditingController communeController = TextEditingController();
  TextEditingController activite_principaleController = TextEditingController();
  TextEditingController activite_secondaireController = TextEditingController();
  String numberWhatsapp="00000000";
  bool phoneNumberEqualWhatsapp=true;
  TextEditingController whatsapp=TextEditingController();
  String code ="+229";
  String namePartenaire ;
  int idPartenaire =0;
  List<String> partenaireList= [];
  String UserPartenaireName = "";
  List<String> groupeEthniqueAppartenance = [
    "Fon",
    "Bariba",
    "Nago",
    "Dendi",
    "Ottamari",
    "Peulh",
    "Yoa",
    "Etranger",
    "Autres"
  ];

  List<String> communes = [
    "Banikoara",
    "Gogounou",
    "Kandi",
    "Karimama",
    "Malanville",
    "Segbana",
    "Boukoumbé",
    "Cobly",
    "Kérou",
    "Kouandé",
    "Matéri",
    "Natitingou",
    "Pehonko",
    "Tanguiéta",
    "Toucountouna",
    "Abomey-Calavi",
    "Allada",
    "Kpomassè",
    "Ouidah",
    "Sô-Ava",
    "Toffo",
    "Tori-Bossito",
    "Zè",
    "Bembéréké",
    "Kalalé",
    "N'Dali",
    "Nikki",
    "Parakou",
    "Pèrèrè",
    "Sinendé",
    "Tchaourou",
    "Bantè",
    "Dassa-Zoumè",
    "Glazoué",
    "Ouèssè",
    "Savalou",
    "Savè",
    "Aplahouè",
    "Djakotomey",
    "Dogbo-Tota",
    "Klouékanmè",
    "Lalo",
    "Toviklin",
    "Bassila",
    "Copargo",
    "Djougou",
    "Ouaké",
    "Cotonou",
    "Athiémé"
        "Bopa",
    "Comè",
    "Grand-Popo",
    "Houéyogbé",
    "Lokossa",
    "Adjarra",
    "Adjohoun",
    "Aguégués",
    "Akpro-Missérété",
    "Avrankou",
    "Bonou",
    "Dangbo",
    "Porto-Novo",
    "Sèmè-Kpodji",
    "Adja-Ouèrè",
    "Ifangni",
    "Kétou",
    "Pobè",
    "Sakété",
    "Abomey",
    "Agbangnizoun",
    "Bohicon",
    "Covè",
    "Djidja",
    "Ouinhi",
    "Zangnanado",
    "Za-Kpota",
    "Zogbodomey",
  ];
  bool _validate = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sampleDataWhatsapp.add(new RadioModelWhatsapp(false, '', 'Oui',));
    sampleDataWhatsapp.add(new RadioModelWhatsapp(false, '', 'Non',));
    setState(() {
      partenaireList = partenaireHelp.getNamePartenaire();
    });
    userRepo.getCurrentUser().then((value) {
      setState(() {
        relais = value;

      });
    });

    UserCtr().getPartenaire().then((value) {
      value.forEach((element) {
        if(element["id"] == relais.partenaire_id){
          setState(() {
            UserPartenaireName=element["intitule"];
          });
          print("Le nom du partenaire est $UserPartenaireName");
        }else
        {
          print("Aucun resultat trouvé");
        }
      });
    });

   // print(relais);

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: (widget.id != 0)? AppBar(title: Text("Profile", style: TextStyle(color: Colors.white, fontFamily: "OSemiBold", fontSize: 20),),):AppBar(title: Text("Renseignements", style: TextStyle(color: Colors.white, fontFamily: "OSemiBold", fontSize: 20),),),
      key: _scaffoldKey,
      body: ListView(
        children: [
         /* (widget.id==0)?Container(
            height: longueurPerCent(125, context),
            padding: EdgeInsets.only(top: 75, left: 20),
            color:  HexColor("#119600"),
            child: Text("Renseignements", style: TextStyle(color: Colors.white, fontFamily: "OSemiBold", fontSize: 20),),
          ):SizedBox(),*/
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: [
                (widget.id == 0)?phonePart():SizedBox(),
                (phoneNumberEqualWhatsapp==false)?Padding(padding: EdgeInsets.symmetric(horizontal: largeurPerCent(20, context)),
                  child: Center(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          flex:2,
                          child: Container(
                            height: 48,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(7)),
                              border: Border.all(color: HexColor("#707070")),
                              color: Colors.white,
                            ),
                            child: CountryCodePicker(
                              onChanged: (e)  {
                                print(e.code.toString());
                                code = e.code.toString();
                              },
                              initialSelection: 'BJ',
                              showCountryOnly: true,
                              showOnlyCountryWhenClosed: false,
                              favorite: ['+229', 'FR'],
                            ),
                          ),
                        ),
                        Expanded(
                            flex: 1,
                            child: Text("")),
                        Expanded(
                          flex: 4,
                          child: Container(
                            height: 48,
                            color: HexColor("#FFFFFF"),
                            child: TextField(
                              style: TextStyle(
                                  color: HexColor("#001C36"),
                                  fontSize: 18,
                                  fontFamily: "OSemiBold"
                              ),
                              keyboardType: TextInputType.number,
                              controller: whatsapp,
                              decoration:  InputDecoration(
                                  contentPadding: EdgeInsets.only(left: 10),
                                  hintText: "Entrez votre numéro mobile",
                                  hintStyle: TextStyle(color: HexColor("#707070"), fontFamily: "ORegular", fontSize: 12),
                                  border: OutlineInputBorder(borderRadius:  BorderRadius.all(Radius.circular(7)), borderSide: BorderSide(color: HexColor("#707070")))
                              ),
                              onChanged: (value){
                                numberWhatsapp= code + value;
                              },
                            ),
                          ),
                        )
                      ],
                    ),
                  ),):Text(("")),
                (phoneNumberEqualWhatsapp==false)? SizedBox(height: longueurPerCent(35, context),):Text(""),

                Column(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(" Nom", style: TextStyle( fontSize: 15),),
                    ),
                    SizedBox(height: 10,),
                    Container(
                        height: 55,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(7.0),
                            ),
                            border: Border.all(
                                color: HexColor("#919191"), width: 1)),
                        child: textField(
                          (widget.id == 0 || relais.last_name == null) ?"Nom":relais.last_name,
                          TextInputType.text, nomController,
                        )
                    ),
                  ],
                ),

                SizedBox(height: 20,),
              Column(
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(" Prénoms", style: TextStyle( fontSize: 15),),
                  ),
                  SizedBox(height: 10,),
                  Container(
                      height: 55,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(7.0),
                          ),
                          border: Border.all(
                              color: HexColor("#919191"), width: 1)),
                      child: textField((widget.id == 0 || relais.last_name == null) ?"Prénoms":relais.first_name, TextInputType.text, prenomsController))
                ],
              )
            ,
                SizedBox(height: 20,),
                Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(" Sexe", style: TextStyle( fontSize: 15),),
                      ),
                      SizedBox(height: 10,),
                      Container(
                        padding: EdgeInsets.only(left: 10, right: 0),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(7.0),
                            ),
                            border: Border.all(
                                color: HexColor("#919191"), width: 1)),
                        child: DropdownButton(
                          hint: sexe == null
                              ? Text(
                            (widget.id == 0 || relais.sexe == null)?'Sexe':relais.sexe,
                            style:TextStyle(color: HexColor("#707070"), fontFamily: "ORegular", fontSize: 15),
                          )
                              : Text(
                            sexe,
                            style: TextStyle(
                                color: HexColor("#001C36"),
                                fontSize: 18,
                                fontFamily: "OSemiBold"
                            ),
                          ),
                          isExpanded: true,
                          underline: Text(
                              ""
                          ),
                          iconSize: 30.0,
                          style: TextStyle(color: HexColor("#919191")),
                          items: ['Masculin', 'Féminin'].map(
                                (val) {
                              return DropdownMenuItem<String>(
                                value: val,
                                child: Text(val, style: TextStyle(
                                    color: HexColor("#001C36"),
                                    fontSize: 18,
                                    fontFamily: "OSemiBold"
                                ),),
                              );
                            },
                          ).toList(),
                          onChanged: (val) {
                            setState(
                                  () {
                                sexe = val;
                              },
                            );
                          },
                        ),
                      )
                    ],
                )
                ,
                SizedBox(height: 20,),
                Column(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(" Ethnie d’appartenance", style: TextStyle( fontSize: 15),),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      padding: EdgeInsets.only(left: 10, right: 0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(7.0),
                          ),
                          border: Border.all(
                              color: HexColor("#919191"), width: 1)),
                      child: DropdownButton(

                        hint: ethnie == null
                            ? Text(
                          (widget.id == 0 || relais.ethnic_group == null)?'Ethnie d’appartenance ':relais.ethnic_group,
                          style: TextStyle(color: HexColor("#707070"), fontFamily: "ORegular", fontSize: 15),
                        )
                            : Text(
                          ethnie,
                          style: TextStyle(
                              color: HexColor("#001C36"),
                              fontSize: 18,
                              fontFamily: "OSemiBold"
                          ),
                        ),
                        isExpanded: true,
                        underline: Text(
                            ""
                        ),
                        iconSize: 30.0,
                        style: TextStyle(color: HexColor("#919191")),
                        items: groupeEthniqueAppartenance.map(
                              (val) {
                            return DropdownMenuItem<String>(
                              value: val,
                              child: Text(val, style: TextStyle(
                                  color: HexColor("#001C36"),
                                  fontSize: 18,
                                  fontFamily: "OSemiBold"
                              ),),
                            );
                          },
                        ).toList(),
                        onChanged: (val) {
                          setState(
                                () {
                              ethnie= val;
                            },
                          );
                        },
                      ),
                    )
                  ],
                )
              ,
                SizedBox(height: 20,),
                Column(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(" Partenaire d'appartenance", style: TextStyle( fontSize: 15),),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      padding: EdgeInsets.only(left: 10, right: 0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(7.0),
                          ),
                          border: Border.all(
                              color: HexColor("#919191"), width: 1)),
                      child: DropdownButton(

                        hint: namePartenaire == null
                            ? Text(
                          (widget.id == 0 || relais.partenaire_id == null)?"Partenaire d'appartenance":UserPartenaireName,
                          style: TextStyle(color: HexColor("#707070"), fontFamily: "ORegular", fontSize: 15),
                        )
                            : Text(
                          namePartenaire,
                          style: TextStyle(
                              color: HexColor("#001C36"),
                              fontSize: 18,
                              fontFamily: "OSemiBold"
                          ),
                        ),
                        isExpanded: true,
                        underline: Text(
                            ""
                        ),
                        iconSize: 30.0,
                        style: TextStyle(color: HexColor("#919191")),
                        items: partenaireList.map(
                              (val) {
                            return DropdownMenuItem<String>(
                              value: val,
                              child: Text(val, style: TextStyle(
                                  color: HexColor("#001C36"),
                                  fontSize: 18,
                                  fontFamily: "OSemiBold"
                              ),),
                            );
                          },
                        ).toList(),
                        onChanged: (val) {
                          setState(
                                () {
                              namePartenaire= val;
                              UserCtr().getPartenaire().then((value) {
                                if(value != null){
                                  value.forEach((element) {
                                    if(element["intitule"] == namePartenaire){
                                      idPartenaire = element["id"];
                                      print("Ici");
                                      print(idPartenaire);
                                    }
                                  });
                                }
                              });
                            },
                          );
                        },
                      ),
                    )
                  ],
                )
                ,
                /*SizedBox(height: 20,),
                Center(
                  child: DropdownSearch<String>(
                    mode: Mode.MENU,
                    maxHeight: 450,
                    hint: (widget.id==0 || relais.commune==null)?"Commune":relais.commune,
                    items: communes.toList(),

                    onChanged: (value) {
                      setState(() {
                        commune = value;
                      });

                    },
                    selectedItem: commune,
                    showClearButton: true,
                    showSearchBox: true,
                    dropdownSearchDecoration: InputDecoration(
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.only(left: longueurPerCent(50, context), right: longueurPerCent(50, context)),
                        hintText: "Rechercher une commune",
                        hintStyle: TextStyle(color: HexColor("#707070"), fontFamily: "ORegular", fontSize: 15)
                    ),
                    popupTitle: Container(
                      height: longueurPerCent(50, context),
                      decoration: BoxDecoration(
                        color: HexColor("#119600"),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20),
                        ),
                      ),
                      child: Center(
                        child: Text(
                          'Communes',
                          style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    popupShape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(7),
                        topRight: Radius.circular(7),
                      ),
                    ),
                  ),
                ),*/
                SizedBox(height: 20,),
                Column(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(" Commune", style: TextStyle( fontSize: 15),),
                    ),
                    SizedBox(height: 10,),
                    Container(
                        height: 55,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(7.0),
                            ),
                            border: Border.all(
                                color: HexColor("#919191"), width: 1)),
                        child: textField((widget.id==0 || relais.commune==null)?"Commune":relais.commune, TextInputType.text, communeController))
                  ],
                )
               ,
                SizedBox(height: 20,),
                Column(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(" Village", style: TextStyle( fontSize: 15),),
                    ),
                    SizedBox(height: 10,),
                    Container(
                        height: 55,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(7.0),
                            ),
                            border: Border.all(
                                color: HexColor("#919191"), width: 1)),
                        child: textField((widget.id==0 || relais.village==null)?"Village":relais.village, TextInputType.text, numArrondissementController))
                  ],
                )
        ,
                SizedBox(height: 20,),
                Column(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(" Activité principale", style: TextStyle( fontSize: 15),),
                    ),
                    SizedBox(height: 10,),
                    Container(
                        height: 55,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(7.0),
                            ),
                            border: Border.all(
                                color: HexColor("#919191"), width: 1)),
                        child: textField((widget.id==0 || relais.activite_principale==null)?"Activité principale":relais.activite_principale, TextInputType.text, activite_principaleController))
                  ],
                )
           ,
                SizedBox(height: 20,),
                Column(
                  children: [
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(" Autre activité", style: TextStyle( fontSize: 15),),
                    ),
                    SizedBox(height: 10,),
                    Container(
                        height: 55,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(7.0),
                            ),
                            border: Border.all(
                                color: HexColor("#919191"), width: 1)),
                        child: textField((widget.id==0 || relais.activite_secondaire==null)?"Autre activité":relais.activite_secondaire, TextInputType.text, activite_secondaireController))
                  ],
                )
           ,
                SizedBox(height: 20,),
                Center(
                  child: submitButton(context, "Suivant", (){

                    if(nomController.text !="" && prenomsController.text!="" && numArrondissementController.text!=""
                        && sexe!=null && ethnie!=null && communeController.text!=null && namePartenaire!=null && activite_principaleController!="") {
                      sendRelaisInfoDb();
                    }
                    else if(nomController.text==""){
                      displaySnackBarNom(context, "Veuillez renseigner votre nom", Colors.white);
                    }
                    else if(prenomsController.text=="")
                      {
                        displaySnackBarNom(context, "Veuillez renseigner votre prénom", Colors.white);
                      }
                    else if(communeController.text=="")
                      {
                        displaySnackBarNom(context, "Veuillez renseigner votre commune", Colors.white);
                      }
                    else if(activite_principaleController.text=="")
                      {
                        displaySnackBarNom(context, "Veuillez renseigner votre activité principale", Colors.white);
                      }
                    else {
                      displaySnackBarNom(context, "Veuillez remplir tous les champs", Colors.white);
                    }

                  }),
                ),
              ],
            ),
          ),
          SizedBox(height: longueurPerCent(20, context),),

        ],
      ),
    );
  }


  Widget phonePart(){
    return Column(
      children: [
        SizedBox(height: longueurPerCent(20, context),),
        displayTextQuestions("Est-ce que le (numéro fourni) est utilisé sur WhatsApp ?", "ORegular", 14, 2, TextAlign.center),
        SizedBox(height: longueurPerCent(10, context),),
        TextClasse(maxLines: 1,textAlign: TextAlign.center, text: widget.phone, color: HexColor("#000000"), fontSize: 14, family: "OSemiBold", fontWeight: FontWeight.bold,),
        SizedBox(height: longueurPerCent(20, context),),
        Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              InkWell(
                splashColor: Colors.blueAccent,
                onTap: () {
                  setState(() {
                    sampleDataWhatsapp.forEach(
                            (element) => element.isSelected = false);
                    sampleDataWhatsapp[0].isSelected = true;
                    phoneNumberEqualWhatsapp=true;
                    numberWhatsapp = widget.phone;
                    print(widget.phone);
                  });
                },
                child: new RadioWhatsapp(sampleDataWhatsapp[0]),
              ),
              SizedBox(width: 10,),
              InkWell(
                splashColor: Colors.blueAccent,
                onTap: () {
                  setState(() {
                    sampleDataWhatsapp.forEach(
                            (element) => element.isSelected = false);
                    sampleDataWhatsapp[1].isSelected = true;
                    phoneNumberEqualWhatsapp=false;
                  });
                },
                child: new RadioWhatsapp(sampleDataWhatsapp[1]),
              )
            ],
          ),
        ),
        SizedBox(height: longueurPerCent(20, context),),
        (phoneNumberEqualWhatsapp==false)?displayTextQuestions("Si le numéro n’est pas utilisé sur WhatsApp, quel est celui utilisé ? (par défaut: 00000000)", "ORegular", 14, 2, TextAlign.center):Text(""),
        (phoneNumberEqualWhatsapp==false)?SizedBox(height: longueurPerCent(10, context),):Text(""),
      ],
    );
  }

  void sendRelaisInfoDb(){
    EasyLoading.show(status: 'Chargement', dismissOnTap: false);
    if(widget.id==0)
    userRepo.login(Relais(
        last_name: nomController.text,
        first_name: prenomsController.text,
        ethnic_group: ethnie,
        sexe: sexe,
        commune: communeController.text,
        village: numArrondissementController.text,
        phone: widget.phone,
        whatsapp: numberWhatsapp,
        language: widget.language,
        partenaire_id: idPartenaire,
        activite_principale: activite_principaleController.text,
        activite_secondaire: activite_secondaireController.text
    )).then((value) async {

      if(value!=null) {
        SharedPreferences sharedPreferences =
            await SharedPreferences.getInstance();
        sharedPreferences.setInt(
            "user_id", value);
        userRepo.setCurrentUser(Relais(
            relais_id: value,
            last_name: nomController.text,
            first_name: prenomsController.text,
            ethnic_group: ethnie,
            sexe: sexe,
            commune: communeController.text,
            village: numArrondissementController.text,
            phone: widget.phone,
            whatsapp: numberWhatsapp,
            language: widget.language,
            partenaire_id: idPartenaire,
            activite_principale: activite_principaleController.text,
            activite_secondaire: activite_secondaireController.text
        ));
        UserCtr().insertUser(Relais(
            relais_id: value,
            last_name: nomController.text,
            first_name: prenomsController.text,
            ethnic_group: ethnie,
            sexe: sexe,
            commune: communeController.text,
            village: numArrondissementController.text,
            phone: widget.phone,
            whatsapp: numberWhatsapp,
            language: widget.language,
            partenaire_id: idPartenaire,
            activite_principale: activite_principaleController.text,
            activite_secondaire: activite_secondaireController.text
        ));
        print(sharedPreferences.getInt("language"));
      }

    });
    else
    userRepo.updateUser(Relais(
        last_name: nomController.text,
        first_name: prenomsController.text,
        ethnic_group: ethnie,
        sexe: sexe,
        commune: communeController.text,
        village: numArrondissementController.text,
        phone: widget.phone,
        whatsapp: numberWhatsapp,
        language: widget.language,
        partenaire_id: idPartenaire,
        activite_principale: activite_principaleController.text,
        activite_secondaire: activite_secondaireController.text
    )).then((value) async {

      if(value!=null) {
        SharedPreferences sharedPreferences =
            await SharedPreferences.getInstance();
        sharedPreferences.setInt(
            "user_id", value);
        userRepo.setCurrentUser(Relais(
            relais_id: value,
            last_name: nomController.text,
            first_name: prenomsController.text,
            ethnic_group: ethnie,
            sexe: sexe,
            commune: commune,
            village: numArrondissementController.text,
            phone: widget.phone,
            whatsapp: numberWhatsapp,
            language: widget.language,
            partenaire_id: idPartenaire,
            activite_principale: activite_principaleController.text,
            activite_secondaire: activite_secondaireController.text
        ));
        UserCtr().insertUser(Relais(
            relais_id: value,
            last_name: nomController.text,
            first_name: prenomsController.text,
            ethnic_group: ethnie,
            sexe: sexe,
            commune: communeController.text,
            village: numArrondissementController.text,
            phone: widget.phone,
            whatsapp: numberWhatsapp,
            language: widget.language,
            partenaire_id: idPartenaire,
            activite_principale: activite_principaleController.text,
            activite_secondaire: activite_secondaireController.text
        ));
        print(sharedPreferences.getInt("language"));
      }

    });

    EasyLoading.dismiss();
    EasyLoading.showSuccess("Enregristrement Réussie");
    EasyLoading.dismiss();

    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => HomeScreen()
      ),
    );
  }

  displaySnackBarNom(BuildContext context, String text, Color couleur) {
    final snackBar = SnackBar(
      content: Text(text, style: TextStyle(color: couleur, fontSize: 13)),
      duration: Duration(seconds: 2),
    );
    // ignore: deprecated_member_use
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }


  bool validateTextField(String nom) {
    if (nom.isEmpty) {
      setState(() {
        _validate = true;
      });
      return false;
    }
    setState(() {
      _validate = false;
    });
    return true;
  }

  Widget textField(String hintText, TextInputType input, TextEditingController controller){
    return  TextField(
      controller: controller,
      style:TextStyle(
          color: HexColor("#001C36"),
          fontSize: 18,
          fontFamily: "OSemiBold"
      ),
      keyboardType: input,
      maxLines: null,
      decoration:  InputDecoration(
          //errorText: _validate ? 'Value Can\'t Be Empty' : null,
          contentPadding: EdgeInsets.only(left: 10),
          hintText: hintText,
          hintStyle: TextStyle(color: HexColor("#707070"), fontFamily: "ORegular", fontSize: 15),
          border: InputBorder.none

      ),

    );
  }

  Padding displayTextQuestions(String text, String family, double fontSize, int maxLines, TextAlign align, ){
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: largeurPerCent(20, context)),
      child: TextClasse(maxLines: maxLines,textAlign: align, text: text, color: HexColor("#000000"), fontSize: 14, family: family,),
    );
  }
}


class RadioModelWhatsapp {
  bool isSelected;
  final String buttonText;
  final String text;

  RadioModelWhatsapp(this.isSelected, this.buttonText, this.text);
}

class RadioWhatsapp extends StatelessWidget {
  final RadioModelWhatsapp _item;
  RadioWhatsapp(this._item);
  @override
  Widget build(BuildContext context) {
    return new Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new Container(
          height: 20.0,
          width: 20.0,
          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
          child: new Center(
            child: new Text(_item.buttonText,
                style: new TextStyle(
                  color:
                  _item.isSelected ? Colors.white : Colors.red,fontSize: 16,
                  fontFamily: "MonseraRegular",
                  //fontWeight: FontWeight.bold,
                )),
          ),
          decoration: new BoxDecoration(
            color: _item.isSelected
                ?  HexColor("#119600")
                : Colors.transparent,
            border: new Border.all(
                width: 1.0,
                color: _item.isSelected
                    ? Colors.blueAccent
                    : Colors.grey),
            borderRadius: const BorderRadius.all(const Radius.circular(25.0)),
          ),
        ),
        SizedBox(width: 5,),
        Text(
          _item.text,
          maxLines: 1,
          style: TextStyle(
              color: HexColor("#000000"),
              fontSize:12,
              fontFamily:
              "ORegular"),
        )
      ],
    );
  }
}
