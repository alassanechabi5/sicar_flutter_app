import 'package:flutter/material.dart';
import 'package:sicar/components/text_classe.dart';
import 'package:sicar/database/controller_db.dart';
import 'package:sicar/models/evaluation.dart';
import 'package:sicar/models/relais.dart';
import 'package:sicar/user_repository.dart' as userRepo;
import 'package:url_launcher/url_launcher.dart';

class EvaluationList extends StatefulWidget {

  @override
  _EvaluationListState createState() => _EvaluationListState();
}

class _EvaluationListState extends State<EvaluationList> {
  List<Evaluation> evaluationList = [];
  bool isEmpty;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    UserCtr().getEvaluation().then((value) {
      print("Ici");
      print(value.isNotEmpty);
      if(value.isNotEmpty)
        value.forEach((element) {
          setState(() {
            isEmpty = false;
            evaluationList.add(element);
          });
        });
      else
        setState(() {
          isEmpty = true;
        });
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            centerTitle: true,
            title: TextClasse(
              text: "Evaluations",
              fontSize: 20,
              family: "OsemiBold",
              textAlign: TextAlign.center,
              color: Colors.white,
            )),
        body:  body()
    );
  }


  Widget body(){
    if(evaluationList.length !=0){
      return ListView.separated(
        separatorBuilder: (context, index) {
          return Column(
            children: [
              Divider(
                height: 10,
                color: Colors.black,
              ),
              SizedBox(
                height: 15,
              ),
            ],
          );
        },
        itemCount: evaluationList.reversed.length,
        itemBuilder: (context, index) {
          return ListTile(
            trailing: TextClasse(
              text: evaluationList[index].date.toString().substring(0,16),
              fontSize: 18,
              family: "ORegular",
              color: Colors.black,
            ),
            title: TextClasse(
              text: evaluationList[index].note_evaluation.toString(),
              fontSize: 18,
              family: "OsemiBold",
              color: Colors.black,
            ),
            leading: TextClasse(
              text: evaluationList[index].cva_name.toUpperCase(),
              fontSize: 18,
              family: "ORegular",
              color: Colors.black,
            ),
          );
        },
      );
    } else if(evaluationList.length == 0 && isEmpty ==null)
      return Center(child: CircularProgressIndicator(),);
      else if(isEmpty == true)
        return Center(
          child: TextClasse(
            text:"Pas d'évaluation effectuée",
            fontSize: 20,
            family: "ORegular",
            color: Colors.black,
          ),
        );
    }
  }


