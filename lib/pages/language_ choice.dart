import 'dart:typed_data';
import 'package:flutter/services.dart';

import 'package:audioplayers/audioplayers.dart';
import 'package:auto_animated/auto_animated.dart';
import 'package:flutter/material.dart';
import 'package:sicar/components/constants_path.dart';
import 'package:sicar/components/hexadecimal.dart';
import 'package:sicar/components/text_classe.dart';
import 'package:shared_preferences/shared_preferences.dart';
/*import 'package:audioplayers/audio_cache.dart';*/
import 'package:sicar/database/controller_db.dart';
import 'package:sicar/pages/informations1.dart';

import '../helper/helper.dart';

class LanguageChoice extends StatefulWidget {
  String phone;
  int id;

  LanguageChoice({this.phone, this.id});

  @override
  _LanguageChoiceState createState() => _LanguageChoiceState();
}

class _LanguageChoiceState extends State<LanguageChoice> {
  AudioCache cache = new AudioCache(prefix: 'lib/audios/');
  AudioPlayer player = AudioPlayer();
  String audioasset = "audios/french.mp3";

  //static const String FRANCAISAUDIO="french.mp3";

  static const String BIALIAUDIO="biali.mp3";

  static const String BARIBAAUDIO="bariba.mp3";

  AudioPlayer player1 = AudioPlayer();
  AudioPlayer player2 =AudioPlayer();
  AudioPlayer player3 =AudioPlayer();
  bool play1 = true;
  bool play2 = true;
  bool play3 = true;
  List<String> langues = [];
  List<int> id = [];

  final options = LiveOptions(
    delay: Duration(seconds: 1),
    showItemInterval: Duration(milliseconds: 922),
    showItemDuration: Duration(seconds: 3),
    visibleFraction: 0.05,
    reAnimateOnVisibility: false,
  );

  Future<void> playAudio() async {

    player1 = await cache.play(PathImagesAudios.FRANCAISAUDIO);

    player1.onPlayerCompletion.listen((event) async {
      setState(() {
        play1=false;
      });
      player2 = await cache.play(BIALIAUDIO);
      player2.onPlayerCompletion.listen((event) async {
        setState(() {
          play2=false;
        });
        player3 = await cache.play(BARIBAAUDIO);
        player3.onPlayerCompletion.listen((event) {
          setState(() {
            play3 = false;
          });
        });
      });
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    UserCtr().getLangue().then((value) {
      value.forEach((element) {
        setState(() {
          langues.add(element.intitule);
          id.add(element.id);
        });
      });
    });
    playAudio();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: (langues.length!=0)?Padding(
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 10,
            ),
            IconButton(
                icon: Icon(
                  Icons.play_circle_fill_outlined,
                  size: 40,
                ),
                onPressed: () {
                  if (play1 == true) {
                    player1.stop();
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => LanguageChoice(
                              phone: widget.phone,
                            )));
                  } else
                  if (play2 == true) {
                    player2.stop();
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => LanguageChoice(
                              phone: widget.phone,
                            )));
                  }  else
                  if (play3 == true) {
                    player3.stop();
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => LanguageChoice(
                              phone: widget.phone,
                            )));
                  } else {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => LanguageChoice(
                              phone: widget.phone,
                              id: widget.id,
                            )));
                  }
                }),
            SizedBox(
              height: 20,
            ),
            LiveGrid.options(
              shrinkWrap: true,
              options: options,
              itemBuilder: buildAnimatedItem,
              itemCount:langues.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 16,
                mainAxisSpacing: 16,
              ),
            ),
          ],
        ),
      ):Center(child: CircularProgressIndicator(),)
    );
  }

  // Build animated item (helper for all examples)
  Widget buildAnimatedItem(
    BuildContext context,
    int index,
    Animation<double> animation,
  ) => FadeTransition(
        opacity: Tween<double>(
          begin: 0,
          end: 1,
        ).animate(animation),
        // And slide transition
        child: SlideTransition(
            position: Tween<Offset>(
              begin: Offset(0, -0.1),
              end: Offset.zero,
            ).animate(animation),
            // Paste you Widget
            child: InkWell(
              onTap: () async {
                if (play1 == true) {
                  player1.stop();
                } else
                if (play2 == true) {
                  player2.stop();
                }  else
                if (play3 == true) {
                  player3.stop();
                }
                SharedPreferences sharedPreferences =
                    await SharedPreferences.getInstance();
                sharedPreferences.setInt(
                    "language", id[index]);
                print(sharedPreferences.getInt("language"));
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Informations1(
                          id: widget.id,
                            phone: widget.phone,
                            language: Helper.getLanguage()[index])));
              },
              child: Container(
                height: 30,
                width: double.infinity,
                color: HexColor("#119600"),
                child: Center(
                  child: TextClasse(
                    textAlign: TextAlign.center,
                    text: langues[index],
                    color: Colors.white,
                    fontSize: 15,
                    family: "OSemiBold",
                  ),
                ),
              ),
            )),
      );
}
