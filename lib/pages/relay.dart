import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:sicar/components/hexadecimal.dart';
import 'package:sicar/components/responsive_calcul.dart';
import 'package:sicar/components/text_classe.dart';
import 'package:sicar/database/controller_db.dart';
import 'package:sicar/models/relais.dart';
import 'package:sicar/user_repository.dart' as userRepo;
import 'package:url_launcher/url_launcher.dart';

class Relay extends StatefulWidget {
  static String id = "Relais";

  @override
  _RelayState createState() => _RelayState();
}

class _RelayState extends State<Relay> {
  List<Relais> _relayList = [];
  List<Relais> allRelais = [];
  String commune;
  String village;
  bool chargement = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initRelay();
  }

  void initRelay(){
    _relayList = [];
    allRelais = [];
    setState(() {
      chargement = true;
    });
    UserCtr().getRelais().then((value) {
      value.forEach((element) {
        setState(() {
          _relayList.add(element);
          allRelais.add(element);
        });
      });
    });
    setState(() {
      chargement = false;
    });
  }

  List<String> communes = [
    "Banikoara",
    "Gogounou",
    "Kandi",
    "Karimama",
    "Malanville",
    "Segbana",
    "Boukoumbé",
    "Cobly",
    "Kérou",
    "Kouandé",
    "Matéri",
    "Natitingou",
    "Pehonko",
    "Tanguiéta",
    "Toucountouna",
    "Abomey-Calavi",
    "Allada",
    "Kpomassè",
    "Ouidah",
    "Sô-Ava",
    "Toffo",
    "Tori-Bossito",
    "Zè",
    "Bembéréké",
    "Kalalé",
    "N'Dali",
    "Nikki",
    "Parakou",
    "Pèrèrè",
    "Sinendé",
    "Tchaourou",
    "Bantè",
    "Dassa-Zoumè",
    "Glazoué",
    "Ouèssè",
    "Savalou",
    "Savè",
    "Aplahouè",
    "Djakotomey",
    "Dogbo-Tota",
    "Klouékanmè",
    "Lalo",
    "Toviklin",
    "Bassila",
    "Copargo",
    "Djougou",
    "Ouaké",
    "Cotonou",
    "Athiémé",
    "Bopa",
    "Comè",
    "Grand-Popo",
    "Houéyogbé",
    "Lokossa",
    "Adjarra",
    "Adjohoun",
    "Aguégués",
    "Akpro-Missérété",
    "Avrankou",
    "Bonou",
    "Dangbo",
    "Porto-Novo",
    "Sèmè-Kpodji",
    "Adja-Ouèrè",
    "Ifangni",
    "Kétou",
    "Pobè",
    "Sakété",
    "Abomey",
    "Agbangnizoun",
    "Bohicon",
    "Covè",
    "Djidja",
    "Ouinhi",
    "Zangnanado",
    "Za-Kpota",
    "Zogbodomey",
  ];


  void search() {
    setState(() {
      chargement = true;
    });
    Navigator.pop(context);
    if(village!=null && commune == null){
      setState(() {
        _relayList = [];
      });
      allRelais.forEach((element) {
        String value = element.village.toLowerCase();
        if(value.contains(village.toLowerCase())) {
          print(element.full_name);
          setState(() {
            chargement = false;
            _relayList.add(element);
          });
        } else
          setState(() {
            chargement = false;
            village = null;
          });
      });

    } else if(village == null && commune!=null){
        setState(() {
          _relayList = [];
        });
        allRelais.forEach((element) {
          if(element.commune == commune) {
            setState(() {
              chargement = false;
              _relayList.add(element);
              commune = null;
            });
          } else
            setState(() {
              chargement = false;
              commune = null;
            });
        });

    } else {
        setState(() {
          _relayList = [];
        });
        allRelais.forEach((element) {
          String value = element.village.toLowerCase();
          if(element.commune == commune && value.contains(village.toLowerCase())) {
            setState(() {
              chargement = false;
              _relayList.add(element);
            });
          } else
            setState(() {
              chargement = false;
              village = null;
              commune = null;
            });
        });

    }

  }




  Future<bool> searchDialog(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return new AlertDialog(
              title: Text(
                "Recherche avancée",
                style: Theme.of(context).textTheme.headline6,
                textAlign: TextAlign.center,
              ),
              content: SingleChildScrollView(
                padding: EdgeInsets.symmetric(horizontal: 0, vertical: 5),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(0.0),
                        ),
                        border: Border.all(
                            color: HexColor("#919191"), width: 1)),
                    child: TextField(
                    style:TextStyle(
                        color: HexColor("#001C36"),
                        fontSize: 18,
                        fontFamily: "OSemiBold"
                    ),
                    onChanged: (input) => village = input,
                    decoration:  InputDecoration(
                        contentPadding: EdgeInsets.only(left: 10),
                        hintText: "Entrer le village",
                        hintStyle: TextStyle(color: HexColor("#707070"), fontFamily: "ORegular", fontSize: 15),
                        border: InputBorder.none
                    ),
                ),
                  ),
                    SizedBox(height: 20),

                    Center(
                      child: DropdownSearch<String>(
                        mode: Mode.DIALOG,
                        maxHeight: 450,
                        hint: "Commune",
                        items: communes.toList(),

                        onChanged: (value) {
                          setState(() {
                            commune = value;
                          });
                        },
                        selectedItem: commune,
                        showClearButton: true,
                        showSearchBox: true,
                        dropdownSearchDecoration: InputDecoration(
                            border: OutlineInputBorder(),
                            contentPadding: EdgeInsets.only(left: longueurPerCent(10, context), right: longueurPerCent(10, context)),
                            hintText: "Rechercher une commune",
                            hintStyle: TextStyle(color: HexColor("#707070"), fontFamily: "ORegular", fontSize: 15)
                        ),
                        popupTitle: Container(
                          height: longueurPerCent(50, context),
                          decoration: BoxDecoration(
                            color: HexColor("#119600"),
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20),
                            ),
                          ),
                          child: Center(
                            child: Text(
                              'Communes',
                              style: TextStyle(
                                fontSize: 24,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                        popupShape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(7),
                            topRight: Radius.circular(7),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 10),
                    //IconButton(icon: Icon(Icons.search, size: 80, color: Theme.of(context).primaryColor), onPressed: ()=>search(commune)),
                    IconButton(icon: Icon(Icons.search, size: 40, color: Theme.of(context).primaryColor), onPressed: ()=>search()),
                    SizedBox(height: 10),

                  ],
                ),
              ));
        });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            centerTitle: true,
            title: TextClasse(
              text: "Relais",
              fontSize: 20,
              family: "OsemiBold",
              textAlign: TextAlign.center,
              color: Colors.white,
            ),
          actions: [
            IconButton(icon: Icon(Icons.search, size: 30, color: Colors.white,), onPressed: ()=>searchDialog(context)),
            IconButton(icon: Icon(Icons.wifi_protected_setup, size: 30, color: Colors.white,), onPressed: ()=>initRelay())
          ],
        ),
      body: body(),
    );
  }


  Widget body(){
    if(_relayList.length==0 && chargement){
      return Center(child: CircularProgressIndicator(),);
    } else if (_relayList.length!=0){
      return ListView.separated(
        separatorBuilder: (context, index) {
          return Column(
            children: [
              Divider(
                height: 10,
                color: Colors.black,
              ),
              SizedBox(
                height: 15,
              ),
            ],
          );
        },
        itemCount: _relayList.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: TextClasse(
              text: _relayList[index].full_name,
              fontSize: 18,
              family: "OsemiBold",
              //family: "ORegular",
              color: Colors.black,
            ),
            subtitle: Container(
              child: Row(
                children: <Widget>[
                  Expanded(child: TextClasse(
                    text: _relayList[index].phone,
                    fontSize: 18,
                    family: "ORegular",
                    color: Colors.black,
                  ),),
                  Container(
                    child: TextClasse(
                      text: _relayList[index].commune,
                      fontSize: 18,
                      family: "ORegular",
                      color: Colors.black,
                    ),
                  )
                ],
              )
            ) ,
            trailing: IconButton(
                icon: Icon(Icons.call, size: 30,),
                color: Color(0xFFEF7532),
                onPressed: () {
                  _launchNumber(_relayList[index].phone);
                }),
            /*leading: Icon(
              Icons.person,
              size: 50,
              color: Theme.of(context).primaryColor,
            ),*/
          );
        },
      );
    } else
      return Center(
        child: TextClasse(
          text:"Pas de producteurs trouvés",
          fontSize: 20,
          family: "ORegular",
          color: Colors.black,
        ),
      );
  }

  _launchNumber(String url) async {
    await launch("tel:$url");
  }
}
