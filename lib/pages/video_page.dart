import 'dart:io';

import 'package:flick_video_player/flick_video_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:video_player/video_player.dart';

import '../helper/helper.dart';

class VideoPage extends StatefulWidget {
  String titleCva;
  VideoPage({this.titleCva});
  @override
  _VideoPageState createState() => _VideoPageState();
}

class _VideoPageState extends State<VideoPage> {

  List<FlickManager> flickManagerList = [];
  bool value=false;
  List<File> fileInfoList = [];


  /*@override
  void dispose() {
    flickManager.dispose();
    super.dispose();
  }

  FlickManager flickManager = FlickManager(
  autoPlay: false,
  videoPlayerController: VideoPlayerController.network("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4")
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 200,
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.symmetric(vertical: 50),
          child: FlickVideoPlayer(flickManager: flickManager),
        ),
      ),
    );
  }*/

  VideoPlayerController _controller;
  bool _isVideoLoaded = false;

  @override
  void initState() {
    _downloadAndCacheVideo();
      /*_downloadAndCacheVideo().then((file) {

      });*/
      /* if(fileInfoList.length!=0){
      print("Dans le init state");
      print(fileInfoList.length);
        print("Ici");
        fileInfoList.forEach((element) {
          setState(() {
            flickManagerList.add(FlickManager(videoPlayerController: VideoPlayerController.file(element)));
          });
        });
        /* flickManager1 = FlickManager(
              autoPlay: false,
              videoPlayerController: VideoPlayerController.file(file[0])
          );
          print(file[0].path);
          flickManager2 = FlickManager(
              autoPlay: false,
              videoPlayerController: VideoPlayerController.file(file[1])
          );*/
    }*/
    super.initState();
  }

  @override
  void dispose() {
    flickManagerList.forEach((element) {
      element.dispose();
    });
    super.dispose();
  }
  Future<List<File>> _downloadAndCacheVideo() async {
    try {
      final cacheManager = DefaultCacheManager();

      FileInfo fileInfo;


      Helper.getCvaFormationSelectVideos(widget.titleCva).forEach((element) async {
        print(element);
        fileInfo = await cacheManager.getFileFromCache(element);
        if(fileInfo?.file == null){
          print("il veut le télécharger");
          fileInfo = await cacheManager.downloadFile(element);
        }
        print(fileInfo?.file.path);
        setState(() {
          flickManagerList.add(FlickManager(videoPlayerController: VideoPlayerController.file(fileInfo?.file), autoPlay: false));
        });
        setState(() {
          value=true;
        });
      });


      return fileInfoList;
    } catch (e) {
      throw (e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: (flickManagerList.length== Helper.getCvaFormationSelectVideos(widget.titleCva).length)?
      ListView.separated(
        separatorBuilder: (context , index){
          return SizedBox(height: 30,);
        },
        itemCount: flickManagerList.length,
        itemBuilder: (context, index){
          return    Container(
            height: 200,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.symmetric(vertical: 50),
            child: FlickVideoPlayer(flickManager: flickManagerList[index]),
          );
        },
      ):Center(child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CircularProgressIndicator(),
          SizedBox(height: 30,),
          Center(child: Text("Veuillez patientez pendant le téléchargement s'il vous plait", textAlign: TextAlign.center,))
        ],
      ),)
      /* ListView(
        children: [
          SizedBox(height: 50,),
          Container(
            height: 200,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.symmetric(vertical: 50),
            child: FlickVideoPlayer(flickManager: flickManager1),
          ),
          SizedBox(height: 50,),
          Container(
            height: 200,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.symmetric(vertical: 50),
            child: FlickVideoPlayer(flickManager: flickManager2)
          ),
        ],
      )*/

      /*_controller?.value?.initialized == true
          ? Center(child: Container(
        height: 200,

        child: VideoPlayer(_controller),
      ))
          : Center(
        child: CircularProgressIndicator(),
      ),*/
    );
  }
}
