import 'dart:io';
import 'dart:typed_data';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';

import 'package:sicar/components/constants_path.dart';
import 'package:sicar/components/hexadecimal.dart';
import 'package:sicar/components/text_classe.dart';
import 'package:sicar/pages/about_us.dart';
import 'package:sicar/pages/display_cva.dart';
import 'package:sicar/pages/evaluation.dart';
import 'package:sicar/pages/informations1.dart';
import 'package:sicar/pages/list_evaluation.dart';
import 'package:sicar/pages/phone_authentification.dart';
import 'package:sicar/pages/publicity.dart';
import 'package:sicar/pages/relay.dart';
import 'package:sicar/pages/send%20_difficulty.dart';
import 'package:sicar/user_repository.dart' as userRepo;



// ignore: must_be_immutable
class ProfileSettings extends StatefulWidget {
  String phone;
  String language;
  int id;

  ProfileSettings({this.phone, this.language, this.id});

  @override
  _ProfileSettingsState createState() => _ProfileSettingsState();
}

class _ProfileSettingsState extends State<ProfileSettings> {
  final _auth = FirebaseAuth.instance;
  bool chargement=false;

  @override
  Widget build(BuildContext context) {

    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          //creationHeader(),
          SizedBox(height: 50,),
          drawerItem(
              url:PathImagesAudios.ELEARNING,
              text: "E-Learning",
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => DisplayCva()));
              }),
          drawerItem(
              url: PathImagesAudios.RELAY,
              text: "Producteurs",
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Relay()));
              }),
          drawerItem(
              url: PathImagesAudios.CHAT,
              text: "Discussions",
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => SendDifficulty()));
              }),
          drawerItem(
              url: "lib/images/quiz.png",
              text: "Liste des evaluations",
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => EvaluationList()));
              }),
          Divider(),
          drawerItem(
              url: "lib/images/utilisateur.png",
              text: "Profil",
              onTap: ()  {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Informations1(
                            id: 1,
                            phone: widget.phone,
                            language: widget.language.toString())));
              }),
          drawerItem(
              url: "lib/images/info.png",
              text: "À propos",
              onTap: () {

                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => AboutUs()));
              }),

          drawerItem(
              url: "lib/images/deconnexions.png",
              text: "Deconnexion",
              onTap: ()  {
                userRepo.logout(context);
              }),

          /*drawerBottom(
              icon: Icons.account_circle,
              text: "Profil",
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Informations1(
                            id: 1,
                            phone: widget.phone,
                            language: widget.language.toString())));
              }),
          drawerBottom(
              icon: Icons.info,
              text: "À propos",
              onTap: () {

                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => AboutUs()));
              }),
          drawerBottom(
              icon: Icons.logout,
              text: "Deconnexion",
              onTap: ()  {
                userRepo.logout(context);
              }),*/
        ],
      ),
    );
  }


  Widget drawerItem({String url, String text, GestureTapCallback onTap}) {
    return ListTile(
      title:   Padding(
        padding: EdgeInsets.only(top: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Image.asset(url, height: 40, width: 40,),
            SizedBox(width: 20,),
            TextClasse(text: text, fontSize: 15, family: "OsemiBold", textAlign: TextAlign.center, color: Colors.black,),
          ],
        ),
      ),
      onTap: onTap,
    );
  }

  Widget drawerBottom({IconData icon, String text, GestureTapCallback onTap}) {
    return ListTile(
      title: Row(
        children: <Widget>[
          (chargement && text=="Partager l'application")?CircularProgressIndicator(): Icon(
            icon,
            color:Theme.of(context).primaryColor,
          ),
          Padding(
            padding: EdgeInsets.only(left: 8.0),
            child: Text(
              text,
              style: TextStyle(
                  color: HexColor('#001C36'),
                  fontSize: 16.0,
                  fontFamily: 'Regular'),
            ),
          )
        ],
      ),
      onTap: onTap,
    );
  }


}