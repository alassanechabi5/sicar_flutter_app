import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sicar/components/hexadecimal.dart';
import 'package:sicar/components/text_classe.dart';
import 'package:sicar/database/controller_db.dart';
import 'package:sicar/helper/helper.dart';
import 'package:sicar/models/cvas.dart';
import 'package:sicar/pages/cva%20_modules.dart';
import 'package:sicar/user_repository.dart' as userRepo;

import '../global_config.dart';
import 'chapitre.dart';


class DisplayCva extends StatefulWidget {
  static String id="DisplayCva";
  @override
  _DisplayCvaState createState() => _DisplayCvaState();
}

class _DisplayCvaState extends State<DisplayCva> {

  AudioPlayer audioPlayer = AudioPlayer();
  List<Cvas> cvas = [];
  int idLangue ;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLanguage();
    UserCtr().getCvas().then((value) {
      value.forEach((element) {
        setState(() {
          cvas.add(element);
        });
      });
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          title:  TextClasse(text: "Formations", fontSize: 20, family: "OsemiBold", textAlign: TextAlign.center, color: Colors.white,)
      ),
      body: (cvas.length!=0)?Column(
        children: [
          SizedBox(height: 20,),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: GridView.count(
              primary: true,
              shrinkWrap: true,
              crossAxisCount: 2,
              childAspectRatio: 0.80,
              mainAxisSpacing: 20.0,
              crossAxisSpacing: 20.0,
              children: List.generate(cvas.length, (index) {
                return InkWell(
                  onTap: (){
                   /* List<String> value =  [];
                    cvas.forEach((element) {
                      value.add("0");
                    });
                    userRepo.setLoadingModule(value);*/
                    Navigator.push(context, MaterialPageRoute(
                    builder: (context) => Chapitre(titleCva:cvas[index].intitule , cvaId: cvas[index].id, langueId: idLangue,)
                    ));
                  },
                  child: Card(
                    elevation: 8.0,
                    child: Container(
                      //  margin: EdgeInsets.only(top: 20, left: 10, right: 10, bottom: 20),
                        child: Container(
                            height: 200.0,
                            width: 200.0,
                          child: CachedNetworkImage(
                            imageUrl:Configuration.base_img_video_url + cvas[index].path_img,
                            fit: BoxFit.cover,
                            ),
                          )),
                  )
                );
              }),
            ),
          ),
        ],
      ):Center(child: CircularProgressIndicator(),)
    );
  }


  Future<void> getLanguage() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      idLangue = sharedPreferences.getInt("language");
    });
  }
}
