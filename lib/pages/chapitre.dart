import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:sicar/components/text_classe.dart';
import 'package:sicar/database/controller_db.dart';
import 'package:sicar/models/module.dart';
import 'package:sicar/pages/cva%20_modules.dart';
import 'package:sicar/user_repository.dart' as userRepo;

import '../global_config.dart';



class Chapitre extends StatefulWidget {
  String titleCva;
  int cvaId;
  int langueId;  String imgChapitre = "";


  Chapitre({this.titleCva, this.cvaId, this.langueId});
  @override
  _ChapitreState createState() => _ChapitreState();
}

class _ChapitreState extends State<Chapitre> {

  List<String> chapitre =  [];
  int dejaCharge = 0;
  Module module;
  List<String> imgChapitre = [];
  List<String> imgStorage = [];
  int loadingAlready;



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getModule();
  }

  void getModule()  {
   // print("Récupérations des modules ");
    UserCtr().getModule(widget.cvaId, widget.langueId).then((value) {

      print(value.length);
      if(value.length!=0) {

          value.forEach((element) {
            if(this.mounted)
            setState(() {
              chapitre.add(element.num_chapitre);
            });
          });
        //}
      } else {
        getModule();
      }
    });
  }


  Future<List<File>>
  _downloadAndCacheAudio() async {
    try {
      final cacheManager = DefaultCacheManager();

      FileInfo fileInfo;


      imgChapitre.forEach((element) async {
        print(element);
        fileInfo = await cacheManager.getFileFromCache(Configuration.base_img_video_url+element);
        if(fileInfo?.file == null){
          print("il veut le télécharger");
          fileInfo = await cacheManager.downloadFile(Configuration.base_img_video_url+element);
        }

        setState(() {
          imgStorage.add(fileInfo?.file.path);
        });
      });


    } catch (e) {
      throw (e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title:  TextClasse(text: widget.titleCva, fontSize: 20, family: "OsemiBold", textAlign: TextAlign.center, color: Colors.white,),
          /*actions: [
            IconButton(icon: Icon(Icons.delete, color: Colors.white,), onPressed: ()=> UserCtr().deleteTableModule())
          ],*/
        ),
        body: (chapitre.length!=0)?Center(
          child: ListView.separated(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            separatorBuilder: (context , index){
              return SizedBox(height: 30,);
            },
            itemCount: chapitre.length,
            itemBuilder: (context, index){
              return InkWell(
                onTap: (){
                  EasyLoading.show(status: 'Chargement', dismissOnTap: false);
                  UserCtr().getModuleWithChapiter(widget.cvaId, widget.langueId, chapitre[index].toString()).then((value) {
                    if(value!=null) {
                      EasyLoading.dismiss();
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context) => CvaModules(module: value, titleCva: widget.titleCva, cvaId: widget.cvaId,)
                      ));
                    } else {
                      EasyLoading.dismiss();
                    }

                  });
                },
                child: Container(
                    height: 200,
                    width: 200,
                    margin: EdgeInsets.symmetric(horizontal: 50),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15.0),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey.withOpacity(0.2),
                              spreadRadius: 3.0,
                              blurRadius: 5.0)
                        ],
                        color: Colors.white),
                    child: Column(children: [
                      Container(
                        margin: EdgeInsets.only(top: 60),
                        height: 80.0,
                        width: 80.0,
                        child: Center(child: TextClasse(text: "${chapitre[index]}", fontSize: 30, family: "OsemiBold", textAlign: TextAlign.center, color: Colors.black)),
                      ),
                     // Image.asset(imgStorage[index], height: 200, width: 200,)
                    ])

                ),
              );
            },
          ),
        ):Center(child: CircularProgressIndicator())
    );
  }
}
