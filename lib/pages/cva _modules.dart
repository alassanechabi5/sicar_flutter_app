import 'dart:io';

/*import 'package:audioplayers/audio_cache.dart';*/
import 'package:audioplayers/audioplayers.dart';
import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flick_video_player/flick_video_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:reorderableitemsview/reorderableitemsview.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sicar/components/text_classe.dart';
import 'package:sicar/database/controller_db.dart';
import 'package:sicar/global_config.dart';
import 'package:sicar/models/evaluation.dart';
import 'package:sicar/models/evaluation_network.dart';
import 'package:sicar/models/module.dart';
import 'package:video_player/video_player.dart';
import '../global_config.dart';



import '../helper/helper.dart';
import 'package:sicar/user_repository.dart' as userRepo;




class CvaModules extends StatefulWidget {
  static String id =" Cva Modules";
  String titleCva;
  int cvaId;
  int langueId;
  Module module;



  CvaModules({this.titleCva, this.cvaId, this.langueId, this.module});
  @override
  _CvaModulesState createState() => _CvaModulesState();
}

class _CvaModulesState extends State<CvaModules> {

  List<FlickManager> flickManagerList = [];
  AudioPlayer audioPlayer = AudioPlayer();
  AudioCache audioCache ;
  List<bool> audiosPlayState = [];
  bool value=false;
  List<File> fileInfoList = [];
  Module module;
  List<String> images = [];
  List<String> audios = [];
  List<String> videos = [];
  List<File> audiosPath= [];
  List<bool> playAudios = [];
  List<String> imagesDisplay = [];
  List<Duration> _durationList = [];
  List<Duration> _positionList = [];
  Duration _duration = new Duration();
  Duration _position = new Duration();
  int chargement = 0;
  final CarouselController _controller = CarouselController();
  List<String> imagesMelanges = [];
  List<Widget> _tiles = <Widget>[];
  List<StaggeredTileExtended> _listStaggeredTileExtended =  <StaggeredTileExtended>[];
  int goodAnswerLength = 0;
  int tabIndex = 0;
  int relais_id;
  int indexCarousel =0;
  int element_actif ;




  Future<void> getIdUser() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    print( sharedPreferences.getInt("user_id"));
    setState(() {
      relais_id = sharedPreferences.getInt("user_id");
    });
  }


  @override
  Future<void> initState()  {
    // TODO: implement initState
    super.initState();
    getModule();
    getIdUser();
  }


  @override
  void dispose() {
    flickManagerList.forEach((element) {
      element.dispose();
    });
    playAudios.forEach((element) {
      if(element==true)
        audioPlayer.stop();
    });
    super.dispose();
  }

  void getModule()  {
        setState(() {
          chargement = 10;
          images = widget.module.path_img.split(new RegExp(";"));
          audios = widget.module.path_audio.split(new RegExp(";"));
          videos = widget.module.path_video.split(new RegExp(";"));
          chargement = 30;
          images.removeLast();
          images.forEach((element) {
            imagesDisplay.add("");
            imagesMelanges.add("");
            _tiles.add(EvaluationImagesDisplayWidget(Key(element),element));
            _listStaggeredTileExtended.add(null);
          });
          audios.removeLast();
          videos.removeLast();

          _downloadAndCacheImage().whenComplete(() {
            /*setState(() {
              _tiles.shuffle();
            });*/
          });
          _downloadAndCacheVideo().whenComplete((){
            chargement = 70;
          });
          _downloadAndCacheAudio().whenComplete(() {
            chargement =95;
          });
        });
}



  Future<List<File>> _downloadAndCacheVideo() async {
    try {
      final cacheManager = DefaultCacheManager();

      FileInfo fileInfo;


      videos.forEach((element) async {
        print(element);
        fileInfo = await cacheManager.getFileFromCache(Configuration.base_img_video_url+element);
        if(fileInfo?.file == null){
          print("il veut le télécharger");
          fileInfo = await cacheManager.downloadFile(Configuration.base_img_video_url+element);
        }

        setState(() {
          flickManagerList.add(FlickManager(videoPlayerController: VideoPlayerController.file(fileInfo?.file), autoPlay: false));
        });
        setState(() {
          value=true;
        });
      });

        setState(() {
          chargement = 60;

        });
      return fileInfoList;
    } catch (e) {
      throw (e);
    }
  }


  Future<List<File>> _downloadAndCacheImage() async {
    try {
      final cacheManager = DefaultCacheManager();

      FileInfo fileInfo;


      for(int i=0; i<images.length; i++){

        fileInfo = await cacheManager.getFileFromCache(Configuration.base_img_video_url+images[i]);
        if(fileInfo?.file == null){
          print("il veut le télécharger");
          fileInfo = await cacheManager.downloadFile(Configuration.base_img_video_url+images[i]);
        }
        print(fileInfo?.file.path);

        setState(() {
          imagesMelanges[i]=fileInfo?.file.path;
          imagesDisplay[i]=fileInfo?.file.path;
          _tiles[i]=EvaluationImagesDisplayWidget(Key(fileInfo?.file.path),fileInfo?.file.path);
          _listStaggeredTileExtended[i]=StaggeredTileExtended.count(2, 2);
        });

      }


      return fileInfoList;
    } catch (e) {
      throw (e);
    }
  }

  /*Future<List<File>> _downloadImgChapitre() async {
    try {
      final cacheManager = DefaultCacheManager();

      FileInfo fileInfo;


        fileInfo = await cacheManager.getFileFromCache(Configuration.base_img_video_url+imgChapitre);
        if(fileInfo?.file == null){
          print("il veut le télécharger");
          fileInfo = await cacheManager.downloadFile(Configuration.base_img_video_url+imgChapitre);
        }
        print(fileInfo?.file.path);

        setState(() {
          imgChapitre = fileInfo?.file.path;
        });


      return fileInfoList;
    } catch (e) {
      throw (e);
    }
  }*/



  Future<List<File>> _downloadAndCacheAudio() async {
    try {
      final cacheManager = DefaultCacheManager();

      FileInfo fileInfo;


      audios.forEach((element) async {
        print( Configuration.base_img_video_url  + element);
        fileInfo = await cacheManager.getFileFromCache(Configuration.base_img_video_url + element);
        if(fileInfo?.file == null){
          print("il veut le télécharger");
          fileInfo = await cacheManager.downloadFile(Configuration.base_img_video_url + element);
        }
        print(fileInfo?.file.path);
        final file = File(fileInfo?.file.path);
        setState(() {
          audiosPath.add(file);
          playAudios.add(false);
          _positionList.add(new Duration());
          _durationList.add(new Duration());
        });
      });



      setState(() {
        chargement = 50;

      });
      return fileInfoList;
    } catch (e) {
      throw (e);
    }
  }





  @override
  Widget build(BuildContext context) {
    return   DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            onTap: (index){

              if(index == 2) {
                _tiles.shuffle();
                setState(() {
                    tabIndex = 2;
                  });
              } else if (index == 1)
              setState(() {
                tabIndex = 1;
              });
             else
                setState(() {
                  tabIndex = 0;
                });
            },
            labelColor: Colors.white,
            indicatorColor: Colors.white,
            tabs: [
              Tab(icon: Icon(Icons.image, size: 30,)),
              Tab(icon: Icon(Icons.video_collection_sharp , size: 30)),
              Tab(icon: Icon(Icons.code , size: 30)),
            ],
          ),
          title: TextClasse(text: "${widget.titleCva}", fontSize: 20, family: "OsemiBold", color: Colors.white,),
        ),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: [
            ImagesAudios(),
            Videos(),
            EvaluationWidget()
          ],
        ),
          floatingActionButton: sendButton()
      ),
    );
  }

  void seekToSecond(int second){
    Duration newDuration = Duration(seconds: second);

    audioPlayer.seek(newDuration);
  }


  Widget slider(int index) {
    //print(index);
    var position = _positionList[index].inSeconds.toDouble();
    return Slider(
        value: !playAudios[index]? 0.0 : position,
        min: 0.0,
        max: !playAudios[index]? 0.0:_durationList[index].inSeconds.toDouble(),
        onChanged: (double value) {
          setState(() {
            seekToSecond(value.toInt());
            value = value;
          });},

    );


  }

  Widget FluidSlider(int index){

  }

  List<Image> imagesModules(){
   List<Image> imgs = [];
   imagesMelanges.forEach((element) {
     setState(() {
       imgs.add(Image.file(File(element), fit: BoxFit.cover,));
     });
   });
    return imgs;
  }

  FloatingActionButton sendButton(){
    if(tabIndex == 2){
      return FloatingActionButton(
        backgroundColor: Colors.black,
          elevation: 10.0,
          child: Image.asset('lib/images/quiz.png', width: 50, height: 80,),
          onPressed: () => {
            setState(() {
              goodAnswerLength = 0;

            }),
            for(int i=0; i<images.length; i++){
              // print(_tiles[i].key.toString().substring(3, _tiles[i].key.toString().length - 3)),
              if(_tiles[i].key.toString().substring(3, _tiles[i].key.toString().length - 3) == imagesDisplay[i]) {
                print(i),
                print("est bonne réponse"),
                setState(() {
                  goodAnswerLength ++;

                }),
              }
            },
            _showSelectionDialog(context),
          }
      );
    } else
      return null;
  }

  Widget ImagesAudios(){
    return    ( videos.length == flickManagerList.length && audiosPath.length!=0 && images.length!=0)?ListView(
      children: [
        SizedBox(height: 30,),
        (audiosPath.length!=0 && images.length!=0 && videos.length!=0)? ListView.separated(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          separatorBuilder: (context , index){
            return SizedBox(height: 30,);
          },
          itemCount: audios.length,
          itemBuilder: (context, index){
            return InkWell(
              onTap: (){

                if(playAudios[index]){
                  setState(() {
                    playAudios[index]=false;
                    _durationList[index] = Duration(seconds: 0);
                    _positionList[index] = Duration(seconds: 0);

                  });
                  print("pause audio");
                  audioPlayer.pause();
                } else {
                  playAudios.forEach((element) {
                    if(element==true)
                      audioPlayer.stop();
                  });

                  for(int i=0; i<playAudios.length; i++){
                    if(i!=index)
                      setState(() {
                        playAudios[i]=false;
                      });
                  }

                  setState(() {
                    print("play audio");
                    playAudios[index]=true;
                    _durationList[index] = Duration(seconds: 0);
                    _positionList[index] = Duration(seconds: 0);
                  });

                  audioPlayer.play(audiosPath[index].path, isLocal: true);

                  /*audioPlayer.onDurationChanged = (d) => setState(() {
                    _durationList[index]=d;
                  });*/

                  audioPlayer.onDurationChanged.listen((d) => setState(() => _durationList[index] = d));

                  audioPlayer.onAudioPositionChanged.listen((p) => setState(() => _positionList[index]=p));

                  /*audioPlayer.positionHandler = (p) => setState(() {
                    _positionList[index]=p;
                  });*/

                  audioPlayer.onPlayerCompletion.listen((event) {
                    setState(() {
                      playAudios[index]=false;
                      _duration = Duration(seconds: 0);
                      _position = Duration(seconds: 0);
                    });
                  });
                }
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(width: 10,),
                  /*Expanded(
                        flex: 2,
                        child: TextClasse(text: "Audio n° $index", fontSize: 15, family: "OsemiBold", color: Colors.black,)),*/
                  Expanded(
                    flex: 1,
                    child: Container(
                      alignment: FractionalOffset.center,
                      child: Container(
                        height: 30,
                        width: 30,
                        child: Icon(playAudios[index]?
                        Icons.pause_circle_filled:Icons.play_circle_fill_outlined, color: Color(0xFFEF7532), size: 30,),
                      ),
                    ),
                  ),
                  Expanded(
                      flex: 3,
                      child: slider(index)
                  )
                ],
              ),
            );
          },
        ):Center(child: CircularProgressIndicator(),),
        SizedBox(height: 30,),
        Container(
          height: 300,
          width: MediaQuery.of(context).size.width,
          child: CarouselSlider(
            items: imagesModules(),
            options: CarouselOptions( height: 300, pageSnapping: false, enlargeCenterPage: true, scrollPhysics: NeverScrollableScrollPhysics(),
                onPageChanged: (index, reason) {
                  setState(() {
                    indexCarousel = index;
                  });
                }
            ),
            carouselController: _controller,
          ),
        ),
        SizedBox(height: 20,),
        Row(
          mainAxisAlignment:  (indexCarousel==0)?MainAxisAlignment.center:MainAxisAlignment.spaceBetween,
          children: <Widget>[
            (indexCarousel!=0)?Flexible(
              child: Padding(
                padding: EdgeInsets.only(left: 20),
                child: RaisedButton(
                  color: Colors.green,
                  onPressed: () => _controller.previousPage(),
                  child: Text('←',  style: TextStyle(color: Colors.white, fontSize: 30,  fontWeight: FontWeight.bold)),
                ),
              ),
            ):SizedBox(),
            Flexible(
              child: Padding(
                padding: EdgeInsets.only(right: 20),
                child: RaisedButton(
                  color: Colors.green,
                  onPressed: () => _controller.nextPage(),
                  child: Text('→', style: TextStyle(color: Colors.white, fontSize: 30, fontWeight: FontWeight.bold),
                ),
            ),
              ),
            )],
        ),
        SizedBox(height: 20,),

      ],
    ):Center(child: Padding(
      padding: const EdgeInsets.only(left: 20),
      child: LinearPercentIndicator(
        width: MediaQuery.of(context).size.width - 50,
        animation: true,
        lineHeight: 20.0,
        animationDuration: 2000,
        percent: 0.9,
        center: Text("$chargement%"),
        linearStrokeCap: LinearStrokeCap.roundAll,
        progressColor: Colors.greenAccent,
      ),
    ),);
  }


  Widget Videos(){
    return (flickManagerList.length== videos.length)?
    ListView.separated(
      shrinkWrap: true,
      separatorBuilder: (context , index){
        return SizedBox(height: 30,);
      },
      itemCount: flickManagerList.length,
      itemBuilder: (context, index){
        return    InkWell(
          onTap: (){
            playAudios.forEach((element) {
              if(element==true)
                audioPlayer.stop();
            });
            for(int i=0; i<playAudios.length; i++){
              if(i!=index)
                setState(() {
                  playAudios[i]=false;
                });
            }
          },
          child: Container(
            height: 300,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.symmetric(vertical: 50),
            child: FlickVideoPlayer(flickManager: flickManagerList[index]),
          ),
        );
      },
    ):Center(child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CircularProgressIndicator(),
      ],
      ),
    );
  }

  Widget EvaluationWidget(){
    return ReorderableItemsView(
      onReorder: (int oldIndex, int newIndex) {
        setState(() {
          _tiles.insert(newIndex, _tiles.removeAt(oldIndex));
        });
      },
      crossAxisSpacing: 20.0,
      mainAxisSpacing: 15,
      children: _tiles,
      crossAxisCount: 4,
      isGrid: true,
      staggeredTiles: _listStaggeredTileExtended,
      longPressToDrag: true,
    );
  }


  void checkReponseEvaluation(){
    setState(() {
      goodAnswerLength = 0;
    });
    for (int i = 0; i < images.length; i++) {
      if (_tiles[i].key.toString().substring(3, _tiles[i].key.toString().length - 3) == imagesMelanges[i]) {
        setState(() {
          goodAnswerLength++;
        });
      }
    }
    _showSelectionDialog(context);
  }
  Widget displayImagesEvaluation(Key key,String imagePath){
    return InkWell(
      onTap: () {},
      child: Image.asset(imagePath, fit: BoxFit.cover,),
    );
  }


  Future<void> _showSelectionDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(

              content: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                  //  (goodAnswerLength == images.length)? Image.asset('lib/images/check.png'):Image.asset('lib/images/close.png'),
                    SizedBox(height: 20,),
                    (goodAnswerLength==images.length)?Image.asset("lib/images/happy.png", height: 200,):Image.asset("lib/images/sad.png", height: 200,),
                    //TextClasse(text: "$goodAnswerLength/${images.length}", fontSize: 50, family: "OsemiBold", color:(goodAnswerLength>images.length/2)?Colors.green:Colors.red/*Color(0xFFEF7532)*/,)
                    // Icon((goodAnswerLength == images.length)?Icons.assignment_turned_in:Icons.close, size: 100, color: (goodAnswerLength == images.length)?Colors.green:Colors.red,)
                  ],
                ),
              ),
            actions: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                      onTap: () {
                        Navigator.pop(context);
                        print("Ajout de l'evalaution");
                        UserCtr().insertEvaluation(
                            Evaluation(
                                date: DateTime.now().toString(),
                                cva_id: widget.cvaId,
                                cva_name: widget.titleCva,
                                note_evaluation:"$goodAnswerLength/${images.length}",
                                user_id: relais_id
                            )
                        );

                        UserCtr().insertEvaluationNetwork(
                            EvaluationNetwork(
                                module_id: int.tryParse(widget.module.num_chapitre),
                                langage_id: widget.langueId,
                                note_evaluation:"$goodAnswerLength/${images.length}",
                                relais_id: relais_id
                            )
                        );
                      },
                      child: TextClasse(text: (goodAnswerLength == images.length)?"Fermer":"Réessayer", fontSize: 20, family: "OsemiBold", color:Colors.black/*Color(0xFFEF7532)*/,)),
                ],
              ),
                  SizedBox(height: 50,)
            ],
          );
        });
  }
}


class EvaluationImagesDisplayWidget extends StatelessWidget {
  EvaluationImagesDisplayWidget(Key key, this.imagePath): super(key: key);

  final String imagePath;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: () {},
        child: new Center(
          child: Image.file(File(imagePath), fit: BoxFit.cover, height: 200,)
        ),
      ),
    );
  }
}

