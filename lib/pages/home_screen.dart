
import 'dart:convert';
import 'dart:io';

/*import 'package:audioplayers/audio_cache.dart';*/
import 'package:audioplayers/audioplayers.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:icon_badge/icon_badge.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sicar/components/constants_path.dart';
import 'package:sicar/components/hexadecimal.dart';
import 'package:sicar/components/text_classe.dart';
import 'package:sicar/database/controller_db.dart';
import 'package:sicar/models/checkInternet.dart';
import 'package:sicar/pages/publicity.dart';
import 'package:sicar/pages/relay.dart';
import 'package:sicar/pages/send%20_difficulty.dart';
import 'package:sicar/pages/settings.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:sicar/user_repository.dart' as userRepo;


import 'package:http/http.dart' as http;

import '../global_config.dart';
import '../models/module.dart';
import 'display_cva.dart';
import 'drawer_list.dart';
import 'informations1.dart';
import 'notifications.dart';
import 'package:sicar/models/response_difficulte.dart';


class HomeScreen extends StatefulWidget {
  static String id="Home screen";
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver{

  //AudioCache cache = AudioCache();
  AudioCache cache = new AudioCache(prefix: 'lib/audios/');
  AudioPlayer player;
  int onTapIndexCaroussel2=0;
  List<bool> play = [false, false, false, false];
  List<bool> playCaroussel = [false, false, false, false, false, false];
  int idLangue ;
  String phoneNumber;


  List<String> audiosMenus = [];


List<String> audiosCaroussel = [
  PathImagesAudios.ALL_LANGUES,
  PathImagesAudios.FORMATIONFRANCAIS,
  PathImagesAudios.ALL_LANGUES,
  PathImagesAudios.FORMATIONFRANCAIS,

];
int responses = 0;
  String customPath = '/audio_recorder_';

  String _dir;
  List<ReponseDifficulte> reponses = [];
  List<String> audios = [];

  _initDir() async {
    if (null == _dir) {
      //directory=(await getExternalStorageDirectory()).path;
      _dir = (await getExternalStorageDirectory()).path;
      //print(_dir);
    }
  }

  Future<File> _downloadFile(String url, String fileName) async {
    var req = await http.Client().get(Uri.parse(url));
    var file = File('$_dir/$fileName');
    return file.writeAsBytes(req.bodyBytes);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    getLanguage();
    _initDir();
     userRepo.getCurrentUser().then((value) =>{
       if(value!=null)
         setState(() {
           phoneNumber=value.phone;
         })
     });
    CheckInternet().checkConnection(context);

    userRepo.getResponselist().then((value){
      if(value!=null){
        value.forEach((element) {
          setState(() {
            responses++;
          });

          print(element.date_reponse);
          customPath =customPath+DateTime.now().millisecondsSinceEpoch.toString();
          _downloadFile(Configuration.base_img_video_url  + "/"+element.path_audio, customPath);

          UserCtr().insertReponseDifficulte(ReponseDifficulte(
              user_id: element.user_id,
              difficulte_id: element.difficulte_id,
              local_difficulte_id: element.local_difficulte_id,
              relais_id: element.relais_id,
              description: element.description,
              path_audio:_dir+customPath,
              date_reponse: element.date_reponse
          ));

          UserCtr().updateDifficulte(_dir+customPath, element.local_difficulte_id,element.date_reponse);

          //UserCtr().insertReponseDifficulte(element);
        });
      }
    });

    getModul();


  }


  Future<void> getModul() async {
   int langueid;
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    print( sharedPreferences.getInt("language"));
    setState(() {
      langueid = sharedPreferences.getInt("language");
    });

    userRepo.getModule(langueid).then((value) async {
      if(value!=null) {
        value.forEach((element) {
          UserCtr().insertModule(element);
        });
      }
    }).catchError((e){
      print("Il y a une erreur");
    });
  }

  Future<String> Save_audio(path) async {
    String local_path;
    try {
      final cacheManager = DefaultCacheManager();

      FileInfo fileInfo;

      print( Configuration.base_img_video_url  + "/"+path);
      fileInfo = await cacheManager.getFileFromCache(Configuration.base_img_video_url +"/"+ path);
      if(fileInfo?.file == null){
        fileInfo = await cacheManager.downloadFile(Configuration.base_img_video_url + "/"+ path);
      }
      local_path = fileInfo?.file.path;
      print(fileInfo?.file.path);

    } catch (e) {
      throw (e);
    }
    return local_path;

  }

  @override
  void dispose() {
    // TODO: implement dispose
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
    CheckInternet().listener.cancel();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print('state = $state');
  }

  @override
  Widget build(BuildContext context) {

    return WillPopScope(
      onWillPop:  () {
        return showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
              return AlertDialog(
                // title: Text("Confirm Exit"),
                content: Text("Voulez vous quittez l'application?"),
                actions: <Widget>[
                  FlatButton(
                    child: Text("Oui"),
                    onPressed: () {
                      SystemNavigator.pop();
                    },
                  ),
                  FlatButton(
                    child: Text("Non"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            });},
      child: Scaffold(
         //backgroundColor: HexColor("#03224c"),
        drawer:ProfileSettings(id:1,phone:phoneNumber,language:idLangue.toString()),
        appBar: AppBar(
          centerTitle: true,
          title:  TextClasse(text: "SICAR", fontSize: 30, family: "OsemiBold", textAlign: TextAlign.center, color: Colors.white,),
          actions: [
            IconBadge(
              icon: Icon(Icons.notifications_none, size: 30,),
              itemCount: responses,
              badgeColor: Color(0xFFEF7532),
              itemColor: Colors.white,
              onTap: () {
                userRepo.check_read();
                setState(() {
                  responses = 0;
                });
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => Notifications()
                ));
              },
            ),
            /*IconButton(icon: Icon(Icons.settings, size: 30,), onPressed:()=>      Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Informations1(
                        id: 1,
                        phone: phoneNumber,
                        language: idLangue.toString()))) )*/
          ],
        ),
        body:(audiosMenus.length!=0)? Center(
          child: ListView(
            children: [
              Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [

                      SizedBox(height: 50,),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: GridView.count(
                            mainAxisSpacing: 0.0,
                            crossAxisSpacing: 10.0,
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            crossAxisCount: 2,
                            children: <Widget>[
                              _buildCard(PathImagesAudios.ELEARNING, false, context, 0),
                              //_buildCard(PathImagesAudios.RELAY, false, context, 1),
                              _buildCard(PathImagesAudios.CHAT, false, context, 1),
                              // _buildCard("lib/images/megaphone.png", false, context, 3),
                            ]
                        ),
                      ),
                      SizedBox(height: 100,),
                     // carousselFournisseursBiensEtServices(),
                      // Padding(padding:EdgeInsets.symmetric(horizontal: largeurPerCent(70, context)), child: _buildCard(PathImagesAudios.CHAT, false, context, 2),),
                      //carousselProjetsPartenaires(),
                      SizedBox(height: 20,)
                    ],
                  )
              ),
            ],
          ),
        ):Center(child: CircularProgressIndicator(backgroundColor: Theme.of(context).primaryColor,),),
        floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: Container(
         child: carousselFournisseursBiensEtServices(),
        ),
      ),
    );
  }



  Widget imageCaroussel(String pathImage, bool top){
    return   Container(
      width: double.infinity,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(pathImage),
              fit: BoxFit.cover
          )
      ),
      child: Stack(
        children: [
          Align(
            child:Container(
              decoration: BoxDecoration(
                color:Colors.blue,
                shape: BoxShape.circle,
              ),
              child:Center(
                child: Icon((!top)?Icons.play_arrow:Icons.call, color: Colors.white, size: 25,),
              ),
              height: 50,
            ),
          )
        ],
      ),
    );
  }

  Widget imageCaroussel2(String pathImage, int i){
    return   Container(
      width: double.infinity,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(pathImage),
              fit: BoxFit.cover
          )
      ),
      child: Stack(
        children: [
          Align(
            child:Container(
              decoration: BoxDecoration(
                color:Colors.blue,
                shape: BoxShape.circle,
              ),
              child:Center(
                child: Icon((!playCaroussel[i])?Icons.play_arrow:Icons.pause, color: Colors.white, size: 25,),
              ),
              height: 50,
            ),
          )
        ],
      ),
    );
  }


  _seeMoreButtonCaroussel1(int i){
    if(i==0)
      _launchNumber(PathImagesAudios.NUMEROABREUVOIRS);
    if(i==1)
      _launchNumber(PathImagesAudios.NUMERONEEMBIO);
    if(i==2)
      _launchNumber(PathImagesAudios.NUMEROPEPINIEREDEPIMENT);
    if(i==3)
      _launchNumber(PathImagesAudios.NUMEROPULVERISATEUR16LITRES);
    if(i==4)
      _launchNumber(PathImagesAudios.NUMEROSEMENCESMARAICHERES);
  }


  _seeMoreButtonCaroussel2(int i) async {
    if(playCaroussel[i]){
      setState(() {
        playCaroussel[i]=false;
      });
      player.pause();
    } else {
      for(int i=0; i<play.length; i++){
        if(play[i]==true){
          player.stop();
          setState(() {
            play[i]=false;
          });
        }
      }
      playCaroussel.forEach((element) {
        if(element==true)
          player.stop();
      });
      setState(() {
        playCaroussel[i]=true;
      });
      for(int i=0; i<play.length; i++){
        if(i!=i)
          setState(() {
            playCaroussel[i]=false;
          });
      }
      player = await cache.play(audiosMenus[i]);
      player.onPlayerCompletion.listen((event) {
        print("Laudio est fini");
        print(playCaroussel[i]);
        setState(() {
          playCaroussel[i]=false;
        });
        print(playCaroussel[i]);
      });
    }
  }

  _launchNumber(String url) async {
    await launch("tel:$url");
  }


  Widget carousselFournisseursBiensEtServices() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 200,
      color: Theme.of(context).primaryColor,
      child: Carousel(
        boxFit: BoxFit.cover,
        autoplay: true,
        autoplayDuration: Duration(seconds: 6),
        animationCurve: Curves.linearToEaseOut,
        animationDuration: Duration(seconds: 2),
        dotSize: 10.0,
        dotIncreasedColor: Color(0xFFEF7532),
        dotPosition: DotPosition.bottomCenter,
        dotVerticalPadding: 10.0,
        indicatorBgPadding: 7.0,
        dotBgColor: Colors.red.withOpacity(0),
        onImageTap:(i){
          _seeMoreButtonCaroussel1(i);
          print(i);
        } ,
        images: [
          imageCaroussel(PathImagesAudios.ABREUVOIRS, true),
          imageCaroussel(PathImagesAudios.NEEMBIO, true),
          imageCaroussel(PathImagesAudios.PEPINIEREDEPIMENT, true),
          imageCaroussel(PathImagesAudios.PULVERISATEUR16LITRES, true),
          imageCaroussel(PathImagesAudios.SEMENCESMARAICHERES, true),
        ],
      ),
    );
  }

  Widget carousselProjetsPartenaires() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 200,
      color: Theme.of(context).primaryColor,
      child: Carousel(
        boxFit: BoxFit.cover,
        autoplay: true,
        autoplayDuration: Duration(seconds: 6),
        animationCurve: Curves.linearToEaseOut,
        animationDuration: Duration(seconds: 2),
        dotSize: 10.0,
        dotIncreasedColor: Color(0xFFEF7532),
        dotPosition: DotPosition.bottomCenter,
        dotVerticalPadding: 10.0,
        indicatorBgPadding: 7.0,
        dotBgColor: Colors.red.withOpacity(0),
        onImageTap:(i) async {

          _seeMoreButtonCaroussel2(i);
          print(i);
        } ,
        images: [
          imageCaroussel2(PathImagesAudios.RBT_WAP_IMAGE, 0),
          imageCaroussel2(PathImagesAudios.ONG_ATPF_IMAGE, 1),
          imageCaroussel2(PathImagesAudios.ONG_EVD_IMAGE, 2),
          imageCaroussel2(PathImagesAudios.PAASTIGNAMB_OMG_IMAGE, 3),
          imageCaroussel2(PathImagesAudios.ENABEL, 4),
          imageCaroussel2(PathImagesAudios.HELVETAS, 5),
        ],
      ),
    );
  }


  Future<void> getLanguage() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    print( sharedPreferences.getInt("language"));
    setState(() {
      idLangue = sharedPreferences.getInt("language");
    });

    print(idLangue);

    if(idLangue==1){
      setState(() {

        audiosMenus.add(PathImagesAudios.FR1);
        audiosMenus.add(PathImagesAudios.FR2);
      });
      print(audiosMenus[0]);
      print(audiosMenus[1]);
    } else if(idLangue==2){
      setState(() {
        audiosMenus.add(PathImagesAudios.BI1);
        audiosMenus.add(PathImagesAudios.BI2);
      });
    } else{
      setState(() {
        audiosMenus.add(PathImagesAudios.BA1);
        audiosMenus.add(PathImagesAudios.BA2);
      });
    }
  }



  Widget _buildCard(String imgPath, bool isPlay, context, int index) {
    return Padding(
        padding: EdgeInsets.only(top: 5.0, bottom: 5.0, left: 5.0, right: 5.0),
        child: InkWell(
            onTap: () {
              if(index==0) {
                play.forEach((element) {
                  if(element==true)
                    player.stop();
                });
               /* for(int i=0; i<playCaroussel.length; i++){
                  if(playCaroussel[i]==true){
                    player.stop();
                    setState(() {
                      playCaroussel[i]=false;
                    });
                  }
                }*/
                setState(() {
                  play[index]=false;
                });
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DisplayCva()
                  ),
                );
              }

              else if(index==1) {
                play.forEach((element) {
                  if(element==true)
                    player.stop();
                });
                setState(() {
                  play[index]=false;
                });
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => SendDifficulty()
                  ),
                );
              }

            },
            child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.0),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 3.0,
                          blurRadius: 5.0)
                    ],
                    color: Colors.white),
                child: Column(children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          InkWell(
                            onTap: () async{
                              if(play[index]){
                                setState(() {
                                  play[index]=false;
                                });
                                player.pause();
                              } else {

                                play.forEach((element) {
                                  if(element==true)
                                    player.stop();
                                });
                                setState(() {
                                  play[index]=true;
                                });
                                for(int i=0; i<play.length; i++){
                                  if(i!=index)
                                    setState(() {
                                      play[i]=false;
                                    });
                                }
                                player = await cache.play(audiosMenus[index]);
                                player.onPlayerCompletion.listen((event) {
                                  setState(() {
                                    play[index]=false;
                                  });
                                });
                              }
                            },
                            child: Icon(play[index]?
                            Icons.pause_circle_filled:Icons.play_circle_fill_outlined, color: Color(0xFFEF7532), size: 30,),
                          )
                        /*  isPlay
                              ? Icon(Icons.pause_circle_filled, color: Color(0xFFEF7532))
                              : Icon(Icons.play_circle_fill_outlined,
                              color: Color(0xFFEF7532))*/
                        ]),
                  ),
                  Container(
                      height: 80.0,
                      width: 80.0,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(imgPath),
                              fit: BoxFit.contain))),
                ]))));
  }
}
//   player = await cache.play(PathImagesAudios.ALL_LANGUES);




/*  _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
*/
