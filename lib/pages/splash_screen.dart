import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:in_app_update/in_app_update.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sicar/components/constants_path.dart';
import 'package:sicar/components/custom_dialog.dart';
import 'package:sicar/components/responsive_calcul.dart';
import 'package:sicar/components/text_classe.dart';
import 'package:http/io_client.dart';
import 'package:sicar/database/controller_db.dart';
import 'package:sicar/models/module.dart';
import 'package:sicar/pages/phone_authentification.dart';
import 'package:url_launcher/url_launcher.dart';
import '../components/hexadecimal.dart';
import '../global_config.dart';
import '../test.dart';
import 'home_screen.dart';
import 'package:sicar/user_repository.dart' as userRepo;

import 'language_ choice.dart';

import 'package:http/http.dart' as http;




class SplashScreen extends StatefulWidget {
  static String id="SplashScreen";
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  int erreur = 0;
  bool userCompleteProfil;
  AppUpdateInfo _updateInfo;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  bool currentUser=false;


  Future<User> getUser() async {
    return FirebaseAuth.instance.currentUser;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userRepo.verifyPhone("+22961861183");
    firstConnectionUser();
    getUser().then((value){
      if(value!=null){
        setState(()  {
          currentUser=true;
        });
      }
    });


    StarTimer();
    checkForUpdate();

  }



  // ignore: non_constant_identifier_names
  StarTimer() async {
    var duration = Duration(seconds: 5);
    return Timer(duration, route);
  }

  route () async {
    print("+++++++++++++++++++");
    print(erreur);
    print(currentUser);
    print(userCompleteProfil);
    if(currentUser && userCompleteProfil){
      if(_updateInfo.updateAvailability==true){
        showUpdateDialog();
      } else
      Navigator.push(context, MaterialPageRoute(
          builder: (context) => HomeScreen()
      ));
    }  else if(!currentUser && erreur == 1){
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.remove("first_connection");
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              title: TextClasse(text: "Erreur", family: "OSemiBold",),
              content: TextClasse(text: "Veuillez vérifier votre connexion internet",),
              actions: <Widget>[
                FlatButton(
                  child: TextClasse(text: "Recharger", family: "OSemiBold", fontSize: 15,),
                  textColor: Colors.white,
                  color: HexColor("#119600"),
                  onPressed: () async{
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) => SplashScreen()
                    ));
                  },
                )
              ],
            );
          }
      );
    } else if((!currentUser && erreur == 0) || (currentUser && !userCompleteProfil)){
      Navigator.push(context, MaterialPageRoute(
          builder: (context) => PhoneAuthentification()
          //builder: (context) => LanguageChoice(phone: "+22954143275",  id: 0,)
      ));
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 100,
              width: 100,
              child:   Image.asset(
                PathImagesAudios.LOGO_SICA,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(height: longueurPerCent(20, context),),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextClasse(textAlign: TextAlign.center, text: "Système d'Information et de Conseils pour l'amélioration des revenus", color: HexColor("#119600"), family: "OsemiBold", fontSize: 20,),
            )
          ],
        ),
      ),
    );
  }


  void showUpdateDialog(){
    showDialog(
      context: context,
      builder: (BuildContext context) => CustomDialog(
        title: "Mise à jour",
        description:
        "Une nouvelle mise à jour est disponible, voulez-vous la télécharger maintenant?",
        cancelButton: FlatButton(
          onPressed: (
              ) {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => HomeScreen()));
          },
          child: Text("Plus tard".toUpperCase(),
            style: TextStyle(
                color: HexColor("#001C36"),
                fontSize: 15.0,
                fontFamily: "OSemiBold"
            ),
          ),
        ),
        nextButton: FlatButton(
          onPressed: (
              ) {
            Navigator.pop(context);
            launchPlayStore();
          },
          child: Text("Oui".toUpperCase(),
            style: TextStyle(
                color: HexColor("#001C36"),
                fontSize: 15.0,
                fontFamily: "OSemiBold"
            ),),
        ),
        icon: Icon(Icons.system_update,size: 100,color: HexColor("#001C36")),
      ),
    );
  }

  launchPlayStore() async {
    String url ="https://play.google.com/store/apps/details?id=sicar.org";

    if (await canLaunch(url)) {
      await launch(url);
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomeScreen()));
    } else {
      Navigator.pop(context);
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomeScreen()));
      throw 'Could not launch $url';
    }
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> checkForUpdate() async {
    InAppUpdate.checkForUpdate().then((info) {
      setState(() {
        _updateInfo = info;
      });
    }).catchError((e) => _showError(e));
  }


  void _showError(dynamic exception) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(exception.toString())));
  }


  Future<void> firstConnectionUser() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    if(sharedPreferences.containsKey("user_id")) {
      print("---");
      print("true");
      setState(() {
        userCompleteProfil = true;
      });
    }
    else {
      print("---");
      print("false");
      setState(() {
        userCompleteProfil = false;
      });
    }

    print(userCompleteProfil);

    //Get module


    if(sharedPreferences.containsKey("first_connection")) {
      /// Si l'utilisateur à la connection faire un update des élements de la base de donnée en ligne dans la base de données locale

      print("On fait le update des information");
      userRepo.getRelais().then((value) {
        if(value!=null) {
          value.forEach((element) {
            print("Nous sommes ici");
            print(element.id);
            UserCtr().deleteRelais(element.id);
            UserCtr().insertRelais(element);
          });
        }
      });

      userRepo.getPartenaire().then((value) {
        if(value!=null){
          value.forEach((element) {
            UserCtr().deletePartenaire(element.id);
            UserCtr().insertPartenaire(element);
          });
        }
      });

      userRepo.getLangues().then((value) {
        if(value!=null)
          value.forEach((element) {
            print(element);
            UserCtr().deleteUser(element.id).whenComplete(() {
              UserCtr().insertLangue(element);
            });
          });
      });

      userRepo.getCvas().then((value) {
        if(value!=null)
          value.forEach((element) {
            UserCtr().deleteCva(element.id);
            UserCtr().insertCvas(element);
          });
      });


    }
    else {
      sharedPreferences.setBool("first_connection", true);

      /// Lors de la première connexion de l'utilisateur on insert les valeurs de manière permanente sur son téléphone

      // 1- Insérer les partenaires
      userRepo.getPartenaire().then((value) {

        if(value!=null){
          value.forEach((element) {
            UserCtr().insertPartenaire(element);
          });
        }
      }).catchError((e){
        setState(() {
          erreur=1;
        });
      });


      // 2- Insérer les relais
      userRepo.getRelais().then((value){

        if(value!=null)
          value.forEach((element) {
            UserCtr().insertRelais(element);
          });
      }).catchError((e){
        setState(() {
          erreur=1;
        });
      });


      // 3-) Récuperer les langues de l'application

      userRepo.getLangues().then((value) {
        if(value!=null)
          value.forEach((element) {
            UserCtr().insertLangue(element);
          });
      }).catchError((e){
        setState(() {
          erreur=1;
        });
      });

      //4 Récupération des cvas

      userRepo.getCvas().then((value) {
        if(value!=null)
          value.forEach((element) {
            UserCtr().insertCvas(element);
          });
      }).catchError((e){
        setState(() {
          erreur=1;
        });
      });

    }
  }
}
