import 'dart:io';

/*import 'package:audioplayers/audio_cache.dart';*/
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sicar/components/text_classe.dart';
import 'package:sicar/database/controller_db.dart';
import 'package:sicar/models/difficulte.dart';
import 'package:sicar/models/response_difficulte.dart';
import 'package:sicar/user_repository.dart' as userRepo;

import '../global_config.dart';
import 'package:http/http.dart' as http;
import 'package:sicar/models/checkInternet.dart';


class Notifications extends StatefulWidget {
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {

  List<ReponseDifficulte> reponses = [];
  List<Difficulte> difficulter = [];
  List<String> audios = [];
  List<String> Date = [];
  List<String> Date_reponse = [];

  List<String> audiosStorage = [];
  Duration _duration = new Duration();
  Duration _duration1 = new Duration();
  Duration _position = new Duration();
  Duration _position1 = new Duration();
  List<Duration> _durationList = [];
  List<Duration> _durationList1 = [];
  List<Duration> _positionList = [];
  List<Duration> _positionList1 = [];
  AudioPlayer audioPlayer = AudioPlayer();
  AudioPlayer audioPlayer1 = AudioPlayer();


  AudioCache audioCache ;
  List<bool> audiosPlayState = [];
  List<bool> audiosPlayState1 = [];
  List<bool> playAudios = [];
  List<bool> difficultesplayAudios = [];
  String customPath = '/audio_recorder_';
  String _dir;
  int taille = 0;
  bool chargement = false;
  //Difficulte difficulte =  Difficulte();
  List <String> difficultes = [];
  List <String> difficultesStorage = [];


  _initDir() async {
    if (null == _dir) {
      _dir = (await getExternalStorageDirectory()).path;
    }
  }

  Future<File> _downloadFile(String url, String fileName) async {
    var req = await http.Client().get(Uri.parse(url));
    var file = File('$_dir/$fileName');
    return file.writeAsBytes(req.bodyBytes);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _initDir();
    setState(() {
      chargement = true;
    });
    print("Nous sommes ici");
    UserCtr().getDifficulte().then((value) {
      print(value);
      if(value!=null) {
        difficulter =value;
        value.forEach((element) {
          difficultes.add(element.path_audio);
          audios.add(element.path_response);
           Date.add(element.date_);
           Date_reponse.add(element.date_reponse);
          _positionList1.add(new Duration());
          _durationList1.add(new Duration());

          _positionList.add(new Duration());
          _durationList.add(new Duration());
          setState(() {
            difficultesStorage.add(element.path_audio);
            difficultesplayAudios.add(false);

            audiosStorage.add(element.path_response);
            playAudios.add(false);
          });

        });

      } else {
        print("Pas de difficulte");
      }
      setState(() {
        chargement = false;
      });
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    playAudios.forEach((element) {
      if(element==true)
        audioPlayer.stop();
    });
    difficultesplayAudios.forEach((element) {
      if(element==true)
        audioPlayer1.stop();
    });


  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  AppBar(
          centerTitle: true,
          title:  TextClasse(text: "Notifications", fontSize: 20, family: "OsemiBold", textAlign: TextAlign.center, color: Colors.white,),
        actions: [
      //IconButton(icon: Icon(Icons.search, size: 30, color: Colors.white,), onPressed: ()=>searchDialog(context)),
       IconButton(icon: Icon(Icons.wifi_protected_setup, size: 30, color: Colors.white,), onPressed: ()=>refresh())
    ],
      ),
      body: body()
    );
  }

  Widget body(){
    if( chargement && audios.length==0)
      return Center(child: CircularProgressIndicator(),);
    else if(!chargement && audios.length!=0)
     return Padding(
       padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
       child: ListView.separated(  separatorBuilder: (context, index) {
           return Column(
             children: [
               SizedBox(height: 15,)
             ],
           );
         },
           itemCount: difficultes.length,
           itemBuilder: (ctx, index){
             return Column(
               children: [

                 InkWell(
                   onTap: (){
                     if(difficultesplayAudios[index]){
                       setState(() {

                         for(int i=0; i<playAudios.length; i++){

                               playAudios[i]=false;
                               audioPlayer.stop();

                         }

                         difficultesplayAudios[index]=false;
                         _durationList1[index] = Duration(seconds: 0);
                         _positionList1[index] = Duration(seconds: 0);

                       });
                       print("pause audio");
                       audioPlayer1.pause();
                     } else {
                       difficultesplayAudios.forEach((element) {
                         if(element==true)
                           audioPlayer1.stop();
                       });
                       for(int i=0; i<difficultesplayAudios.length; i++){
                         if(i!=index)
                           setState(() {
                             difficultesplayAudios[i]=false;
                           });
                       }
                       setState(() {
                         print("play audio");
                         difficultesplayAudios[index]=true;
                         _durationList1[index] = Duration(seconds: 0);
                         _positionList1[index] = Duration(seconds: 0);

                       });

                       audioPlayer1.play(difficultesStorage[index], isLocal: true);
                       audioPlayer.stop();

                       audioPlayer1.onDurationChanged.listen((d) => setState(() => _durationList1[index] = d));

                       audioPlayer1.onAudioPositionChanged.listen((p) => setState(() => _positionList1[index]=p));

                       audioPlayer1.onPlayerCompletion.listen((event) {
                         setState(() {
                           difficultesplayAudios[index]=false;
                           _duration1 = Duration(seconds: 0);
                           _position1 = Duration(seconds: 0);
                         });
                       });
                     }
                   },
                   child: Row(
                     mainAxisAlignment: MainAxisAlignment.end,

                     children: [

                       Expanded(
                         flex: 6,
                         child: Container(
                           height: 70,
                           decoration: BoxDecoration(
                             borderRadius: BorderRadius.all(Radius.circular(10.0)),
                             color: Colors.green.withOpacity(0.5),
                           ),
                           child: Column(
                             children: [
                               Row(
                                 children: [
                                   Expanded(
                                     flex: 1,
                                     child: Container(
                                       alignment: FractionalOffset.center,
                                       child: Container(
                                         height: 30,
                                         width: 30,
                                         child: Icon(difficultesplayAudios[index]?
                                         Icons.pause_circle_filled:Icons.play_circle_fill_outlined, color: Color(0xFFEF7532), size: 30,),
                                       ),
                                     ),
                                   ),
                                   Expanded(
                                       flex: 3,
                                       child: slider2(index)),
                                   //Expanded(child: Text(Date[index]))

                                 ],
                               ),
                               Row(
                                 children: [
                                   Container(
                                     child: Text(Date[index]),
                                   )
                                 ],
                               )
                               //   Text("23:10 ", style: TextStyle(color: Colors.white, fontSize: 15, fontFamily: "OsemiBold",), textAlign: TextAlign.right,)
                             ],
                           ),
                         ),
                       ),
                       Expanded(
                           flex: 4,
                           child: Container()),

                       SizedBox(width: 10,),

                     ],
                   ),
                 ),
                 SizedBox(height: 20,),
                 audiosStorage[index]=="Pas de reponse"?Text(""):
                 InkWell(
                   onTap: (){

                     if(playAudios[index]){
                       setState(() {

                         playAudios[index]=false;
                         _durationList[index] = Duration(seconds: 0);
                         _positionList[index] = Duration(seconds: 0);

                       });
                       print("pause audio");
                       audioPlayer.pause();
                     } else {
                       playAudios.forEach((element) {
                         if(element==true)
                           audioPlayer.stop();
                       });
                       for(int i=0; i<playAudios.length; i++){
                         if(i!=index)
                           setState(() {
                             playAudios[i]=false;
                           });
                       }
                       setState(() {
                         print("play audio");
                         playAudios[index]=true;
                         _durationList[index] = Duration(seconds: 0);
                         _positionList[index] = Duration(seconds: 0);
                       });

                       audioPlayer.play(audiosStorage[index], isLocal: true);
                       audioPlayer1.stop();

                       audioPlayer.onDurationChanged.listen((d) => setState(() => _durationList[index] = d));

                       audioPlayer.onAudioPositionChanged.listen((p) => setState(() => _positionList[index]=p));

                       audioPlayer.onPlayerCompletion.listen((event) {
                         setState(() {
                           playAudios[index]=false;
                           _duration = Duration(seconds: 0);
                           _position = Duration(seconds: 0);
                         });
                       });
                     }
                   },
                   child: Row(
                     mainAxisAlignment: MainAxisAlignment.center,
                     children: [
                       SizedBox(width: 10,),
                       Expanded(
                           flex: 4,
                           child: Container()),
                       Expanded(
                         flex: 6,
                         child: Container(
                           decoration: BoxDecoration(
                             borderRadius: BorderRadius.all(Radius.circular(10.0)),
                             color: Colors.transparent.withOpacity(0.5),
                           ),
                           height: 70,
                           child: Column(
                             children: [
                               Row(
                                 children: [
                                   Expanded(
                                     flex: 1,
                                     child: Container(
                                       alignment: FractionalOffset.center,
                                       child: Container(
                                         height: 30,
                                         width: 30,
                                         child: Icon(playAudios[index]?
                                         Icons.pause_circle_filled:Icons.play_circle_fill_outlined, color: Color(0xFFEF7532), size: 30,),
                                       ),
                                     ),
                                   ),
                                   Expanded(
                                       flex: 5,
                                       child: slider1(index)
                                   ),
                                 ],
                               ),
                               Row(
                                 children: [
                                   Container(
                                     child: Text(Date_reponse[index]),
                                   )
                                 ],
                               )
                             ],
                           ),
                         ),
                       ),

                     ],
                   ),

                 ),

               ],
             );
           }
       ),
     );
    else
      return Center(
        child:  Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextClasse(text: "Pas de Notifications", fontSize: 20, family: "OsemiBold", textAlign: TextAlign.center, color: Colors.black,),
            SizedBox(height: 20,),
            Icon(Icons.notifications_none, color: Colors.black, size: 100,)
          ],
        ),
      );
  }

  void seekToSecond(int second){
    Duration newDuration = Duration(seconds: second);

    audioPlayer.seek(newDuration);

  }

  void seekToSecond1(int second){
    Duration newDuration = new Duration(seconds: second);

    audioPlayer1.seek(newDuration);
  }


  Widget slider2(int index) {

    var position = _positionList1[index].inSeconds.toDouble();
    return Slider(
        value:!difficultesplayAudios[index]?0.0: position,
        min: 0.0,
        max: !difficultesplayAudios[index]?0.0:_durationList1[index].inSeconds.toDouble(),
        onChanged: (double value)
        {
          setState(()
          {
            seekToSecond(value.toInt());
            value = value;
          });
        });


  }

  Widget slider1(int index) {

    var position = _positionList[index].inSeconds.toDouble();
    return Slider(
        value: !playAudios[index]?0.0: position,
        min: 0.0,
        max: !playAudios[index]?0.0:_durationList[index].inSeconds.toDouble(),
        onChanged: (double value)
        {
          setState(()
          {
            seekToSecond(value.toInt());
            value = value;
          });
        });


  }
/*        */


  /*Widget slider2() {
    return Slider(

        value:0.0,
        min: 0.0,
        max:10.0,
        activeColor: Colors.blue,
        inactiveColor:Colors.transparent.withOpacity(0.5),
        onChanged: (double value) {

        });
  }*/

  refresh() {

    userRepo.getResponselist().then((value){
      if(value!=null){
        value.forEach((element) {
          customPath =customPath+DateTime.now().millisecondsSinceEpoch.toString();
          _downloadFile(Configuration.base_img_video_url  + "/"+element.path_audio, customPath);

          UserCtr().insertReponseDifficulte(ReponseDifficulte(
              user_id: element.user_id,
              difficulte_id: element.difficulte_id,
              local_difficulte_id: element.local_difficulte_id,
              relais_id: element.relais_id,
              description: element.description,
              path_audio:_dir+customPath,
              date_reponse: element.date_reponse
          ));

          UserCtr().updateDifficulte(_dir+customPath, element.local_difficulte_id,element.date_reponse);


        });
      }
    });
    userRepo.check_read();

    Navigator.pop(context);
    Navigator.push(context, MaterialPageRoute(
        builder: (context) => Notifications()
    ));

    /*Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => super.widget));
*/



      /*userRepo.getResponselist().then((value){
        if(value!=null){
          value.forEach((element) {
            customPath =customPath+DateTime.now().millisecondsSinceEpoch.toString();
            _downloadFile(Configuration.base_img_video_url  + "/"+element.path_audio, customPath);

            UserCtr().insertReponseDifficulte(ReponseDifficulte(
                user_id: element.user_id,
                difficulte_id: element.difficulte_id,
                local_difficulte_id: element.local_difficulte_id,
                relais_id: element.relais_id,
                description: element.description,
                path_audio:_dir+customPath,
                updated_at: element.updated_at
            ));

            UserCtr().updateDifficulte(_dir+customPath, element.local_difficulte_id);

          });
        }
      });*/


  }

}
