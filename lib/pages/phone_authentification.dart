import 'dart:io';

import 'package:country_code_picker/country_code_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/io_client.dart';
import 'package:sicar/components/hexadecimal.dart';
import 'package:sicar/components/responsive_calcul.dart';
import 'package:sicar/components/submit_button.dart';
import 'package:sicar/components/text_classe.dart';
import 'package:sicar/database/controller_db.dart';

import 'home_screen.dart';
import 'language_ choice.dart';


class   PhoneAuthentification extends StatefulWidget {
  static String id ="PhoneAuthentification";


  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<PhoneAuthentification> {
  final _codeController = TextEditingController();
  final _phoneController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String _nameUser;
  String numUser;
  String numero;
  String code="+229";

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();






  Future<bool> loginUser(String phone, BuildContext context) async{
    EasyLoading.show(status: 'Chargement', dismissOnTap: false);
    FirebaseAuth _auth = FirebaseAuth.instance;
    _auth.verifyPhoneNumber(
      phoneNumber: phone,
      timeout: Duration(seconds: 60),
      verificationCompleted: (AuthCredential credential) async{
        EasyLoading.dismiss();
        await _auth.signInWithCredential(credential).then((value) {
          if(value.user!=null){
            Navigator.push(context, MaterialPageRoute(
                builder: (context) => LanguageChoice(phone: code+numero,  id: 0,)
            ));

          } else {
            print("Errorr");
            displaySnackBarNom(context, "Vous n'avez pas réussi à vous inscrire", Colors.red);

          }
        });
      },
      verificationFailed: (FirebaseAuthException exception){
        if (exception.code == 'invalid-phone-number') {
          print('The provided phone number is not valid.');
          displaySnackBarNom(context, "Le numéro entré n'est pas correct", Colors.red);
        }
      },
      codeSent: (String verificationId, [int forceResendingToken]){
        EasyLoading.dismiss();
        showDialog(
            context: context,
            barrierDismissible: false,
            builder: (context) {
              return AlertDialog(
                title: TextClasse(text: "Entrer le code", family: "OSemiBold",),
                content: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      TextFormField(
                        keyboardType: TextInputType.number,
                        style: TextStyle(
                            color: HexColor("#001C36"),
                            fontSize: 18,
                            fontFamily: "OSemiBold"
                        ),
                        controller: _codeController,
                        // ignore: missing_return
                        validator: (String value) {
                          if(value.isEmpty) {
                            return ("Entrer le code de vérification");
                          } else if (value.length!=6) {
                            return ("Le code de vérification est inconrect ");
                          }
                        },
                      ),
                    ],
                  ),
                ),
                actions: <Widget>[
                  FlatButton(
                    child: TextClasse(text: "Confirmer", family: "OSemiBold", fontSize: 15,),
                    textColor: Colors.white,
                    color: HexColor("#119600"),
                    onPressed: () async{
                     if(_formKey.currentState.validate()){
                       EasyLoading.show(status: 'Chargement', dismissOnTap: false);
                       final code = _codeController.text.trim();
                       try {
                         AuthCredential credential = PhoneAuthProvider.credential(verificationId: verificationId, smsCode: code);

                         await _auth.signInWithCredential(credential).then((value) {
                           if(value.user!=null){
                             EasyLoading.dismiss();
                             Navigator.push(context, MaterialPageRoute(
                                 builder: (context) => LanguageChoice(phone: code+numero,  id: 0,)
                             ));

                           } else {
                             print("Error");
                             displaySnackBarNom(context, "Vous n'avez pas réussi à vous inscrire", Colors.red);
                           }
                         });
                       } catch(e){
                         EasyLoading.dismiss();
                         Navigator.pop(context);
                         displaySnackBarNom(context, "Code Invalide. Veuillez reconfirmer votre numéro", Colors.red);
                       }

                     }
                    },
                  )
                ],
              );
            }
        );
      },
      codeAutoRetrievalTimeout:  (String verificationId) {
        // Auto-resolution timed out...
      },
    );
  }





  displaySnackBarNom(BuildContext context, String text, Color couleur) {
    final snackBar = SnackBar(
      content: Text(text, style: TextStyle(color: couleur, fontSize: 13)),
      duration: Duration(seconds: 2),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    UserCtr().getPartenaire().then((value) {
      if(value!=null)
        print(value);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop:  () {
        EasyLoading.dismiss();
        return showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
              return AlertDialog(
                // title: Text("Confirm Exit"),
                content: Text("Voulez vous quittez l'application?"),
                actions: <Widget>[
                  FlatButton(
                    child: Text("Oui"),
                    onPressed: () {
                      SystemNavigator.pop();
                    },
                  ),
                  FlatButton(
                    child: Text("Non"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            });},
      child: Scaffold(
        key: _scaffoldKey,
          body: ListView(
            children: [
              Container(
                height: longueurPerCent(125, context),
                padding: EdgeInsets.only(top: 75, left: 20),
                color:  HexColor("#119600"),
                child: Text("Activez l'application", style: TextStyle(color: Colors.white, fontFamily: "OSemiBold", fontSize: 20),),
              ),
              SizedBox(height: longueurPerCent(100, context),),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    flex:1,
                    child: Text("")
                  ),
                  Expanded(
                    flex:2,
                    child:
                        Column(
                          children: [
                            SizedBox(height: 15,),
                            CountryCodePicker(
                              onChanged: (e)  {
                                print(e.code.toString());
                                code = e.code.toString();
                              },
                              initialSelection: 'BJ',
                              showCountryOnly: true,
                              showOnlyCountryWhenClosed: false,
                              favorite: ['+229', 'FR'],
                            ),
                          ],
                        ),
                  ),
                  Expanded(
                    flex: 3,
                    child: TextFormField(
                      style: TextStyle(
                          color: HexColor("#001C36"),
                          fontSize: 18,
                          fontFamily: "OSemiBold"
                      ),
                      controller: _phoneController,
                      keyboardType: TextInputType.number,
                      autofocus: true,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(left: 10),
                        labelText: "Entrez votre numéro mobile",
                        labelStyle: TextStyle(color: HexColor("#119600"), fontFamily: "ORegular", fontSize: 12)
                      ),
                      onChanged: (value){
                       numero=_phoneController.text;
                      },
                    ),
                  ),
                  Expanded(
                      flex:1,
                      child: Text("")
                  ),
                ],
              ),
              SizedBox(height: longueurPerCent(20, context),),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 42),
                child: Text("Nous vous enverrons un code par message pour vérifier votre numéro de téléphone", style: TextStyle(color: HexColor("#707070"), fontFamily: "ORegular", fontSize: 13, ), textAlign: TextAlign.center,),
              ),
              SizedBox(height: longueurPerCent(200, context),),
              submitButton(context, "Continuer", (){
                if(code!=null && numero!=null){
                  loginUser(code+numero, context);
                } else {
                  displaySnackBarNom(context, "Veuillez entrer votre numéro de téléphone", Colors.white);
                }
              })
            ],
          )
      ),
    );
  }
}