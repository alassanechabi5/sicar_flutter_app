import 'dart:async';
import 'dart:convert';
import 'dart:io' as io;

import 'package:audioplayers/audioplayers.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:chengguo_audio_recorder_v2/audio_recorder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_audio_recorder2/flutter_audio_recorder2.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sicar/components/constants_path.dart';
import 'package:sicar/components/responsive_calcul.dart';
import 'package:sicar/components/text_classe.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sicar/database/controller_db.dart';
import 'package:sicar/models/cvas.dart';
import 'package:sicar/models/difficulte.dart';
import 'package:sicar/user_repository.dart' as userRepo;
import 'dart:io' as Io;

import '../global_config.dart';
import 'home_screen.dart';
import 'dart:typed_data';
import 'package:http/http.dart' as http;
import 'package:file/file.dart';
import 'package:file/local.dart';
import 'package:path_provider/path_provider.dart';


class SendDifficulty extends StatefulWidget {
  final LocalFileSystem localFileSystem;

  SendDifficulty({localFileSystem}) : this.localFileSystem = localFileSystem ?? LocalFileSystem();
  @override
  _SendDifficultyState createState() => _SendDifficultyState();
}

class _SendDifficultyState extends State<SendDifficulty> {
  FlutterAudioRecorder2 _recorder;
  Recording _current =new Recording() ;
  RecordingStatus _currentStatus = RecordingStatus.Unset;
  AudioPlayer audio = AudioPlayer();
  List<int> _value = [];
  int cvaSelect;
  List<Cvas> cvas = [];

  bool _isRecording = false;
  String _audioPath = "";
  Timer _timer;
  int _tick = 0;
  bool gallery = false;
  File _image1;

  bool _isPlay = false;
  bool start_enregistrement = false;
  bool end_enregistrement = false;
  int relais_id;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final picker = ImagePicker();

  var MONTHS = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];

  int last_local_difficulte_id = 0;

  String formattedDateTime() {
    DateTime now = new DateTime.now();
    return now.year.toString()+"-"+MONTHS[now.month-1]+"-"+now.day.toString()+" à "+now.hour.toString()+":"+now.minute.toString()+":"+now.second.toString();
  }

  Future<void> getIdUser() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    print(sharedPreferences.getInt("user_id"));
    setState(() {
      relais_id = sharedPreferences.getInt("user_id");
    });
  }

  _startRecord() async {
    try {


      var isRecording = await AudioRecorder.isRecording();
      var path = await AudioRecorder.startRecord();
      debugPrint("path: $path");
      setState(() {
        _tick = 0;
        _audioPath = path;
        _isRecording = isRecording;
      });
      _timer = Timer.periodic(Duration(seconds: 1), (timer) async {
        double db = await AudioRecorder.db;
        setState(() {
          _tick = timer.tick;

        });
        if (_tick == 30) _stopRecord();
      });
    } catch (e) {
      debugPrint("$e");
    }
  }



  _stop() async {
    var result = await _recorder.stop();
    print("Stop recording: ${result.path}");
    print("Stop recording: ${result.duration}");

    _timer.cancel();
    var isRecording = await AudioRecorder.isRecording();
    setState(() {
      start_enregistrement = false;
      end_enregistrement = true;
      _current = result;
      _currentStatus = _current.status;
      _isRecording = isRecording;
    });

    File file = widget.localFileSystem.file(result.path);
    print("File length: ${await file.length()}");

  }

  _stopRecord() async {
    try {
      await AudioRecorder.stopRecord();
    } catch (e) {
      debugPrint("$e");
    } finally {
      _timer?.cancel();
      var isRecording = await AudioRecorder.isRecording();
      setState(() {
        _isRecording = isRecording;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _init();
    getIdUser();
    UserCtr().getCvas().then((value) {
      value.forEach((element) {
        setState(() {
          cvas.add(element);
          _value.add(0);
        });
      });
    });

    UserCtr().getLastDifficulte().then((value) {
      if(value!=null){
        value.forEach((element) {
          setState(() {
            last_local_difficulte_id = element.id+1;
          });

          print("Voici le dernier id : $last_local_difficulte_id");
          //print(last_local_id);
        });
      }else{
        setState(() {
          last_local_difficulte_id = 1;
        });

      }
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
          title: TextClasse(
        text: "Requête",
        fontSize: 20,
        family: "OsemiBold",
        color: Colors.white,
      )),
      body: Container(
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: longueurPerCent(20, context),
                ),
                (_image1 == null)
                    ? Center(
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 50, horizontal: 20),
                          child: FlatButton(
                              height: 100,
                              onPressed: () {
                                _showSelectionDialog(context);
                              },
                              padding: EdgeInsets.symmetric(vertical: 14),
                              color: Theme.of(context)
                                  .accentColor
                                  .withOpacity(0.2),
                              shape: StadiumBorder(),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(
                                    Icons.add_a_photo,
                                    color: Theme.of(context).accentColor,
                                  ),
                                  SizedBox(
                                    width: 15,
                                  ),
                                  Text(
                                    "PRENDRE UNE PHOTO",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        color: Theme.of(context).accentColor,
                                        fontWeight: FontWeight.w800),
                                  ),
                                ],
                              )),
                        ),
                      )
                    : Card(
                        semanticContainer: true,
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        child: Container(
                          height: 300,
                          child: Stack(
                            fit: StackFit.loose,
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  _showSelectionDialog(context);
                                },
                                child: Image.file(_image1, fit: BoxFit.fill),
                              )
                            ],
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        elevation: 5,
                        margin: EdgeInsets.all(10),
                      ),
                SizedBox(
                  height: 0,
                ),
                Padding(
                  padding:EdgeInsets.symmetric(horizontal: 10),
                  child: GridView.count(
                    primary: true,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    crossAxisCount: 3,
                    childAspectRatio: 0.80,
                    mainAxisSpacing: 20.0,
                    crossAxisSpacing: 20.0,
                    children: List.generate(cvas.length, (index) {
                      return InkWell(
                          onTap: (){
                             if(_value[index] == 1){
                               setState(() {
                                 _value[index]  =0;
                               });
                             } else {
                                for(int i=0; i<_value.length; i++){
                                  setState(() {
                                    _value[i]=0;
                                  });
                                }
                               setState(() {
                                 cvaSelect = cvas[index].id;
                                 _value[index]  =1;
                               });
                             }

                          },
                          child: Container(
                            padding: const EdgeInsets.all(3.0),
                            decoration: BoxDecoration(
                                border: (_value[index]==1)?Border.all(color: Colors.green, width: 5):null
                            ),
                            child: CachedNetworkImage(
                              imageUrl:Configuration.base_img_video_url + cvas[index].path_img,
                              fit: BoxFit.fitHeight,
                            ),
                          )
                      );
                    }),
                  ),
                ),
                SizedBox(height: 15,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TextClasse(
                      text: "$_tick s",
                      fontSize: 30,
                      family: "OsemiBold",
                      color: Colors.black,
                    ),
                    MaterialButton(
                      onPressed: () {
                        switch (_currentStatus) {
                          case RecordingStatus.Initialized:
                            {
                              _start();
                              break;
                            }
                          case RecordingStatus.Recording:
                            {
                              _stop();
                              break;
                            }
                          case RecordingStatus.Paused:
                            {
                              _resume();
                              break;
                            }
                          case RecordingStatus.Stopped:
                            {
                              _init();
                              break;
                            }
                          default:
                            break;
                        }
                        /*if (_isRecording == false)
                          _startRecord();
                          _start();
                        else
                          _stopRecord();*/
                      },
                      child: Container(
                        height: 30,
                        width: 30,
                        child: Icon(
                          ( _currentStatus == RecordingStatus.Initialized || _currentStatus == RecordingStatus.Paused || _currentStatus == RecordingStatus.Stopped)
                              ? Icons.stop_circle_rounded
                              : Icons.settings_voice_rounded,
                          color:
                          ( _currentStatus == RecordingStatus.Initialized || _currentStatus == RecordingStatus.Paused || _currentStatus == RecordingStatus.Stopped) ? Colors.green : Colors.red,
                          size: 40,
                        ),
                      ),
                    ),
                    new InkWell(
                      child: new Container(
                        alignment: FractionalOffset.center,
                        child: (!_isPlay)
                            ? Container(
                                alignment: FractionalOffset.center,
                                child: Container(
                                  height: 30,
                                  width: 30,
                                  child: Icon(
                                    Icons.play_circle_fill_outlined,
                                    size: 40,
                                    color: Color(0xFFEF7532),
                                  ),
                                ),
                              )
                            : Container(
                                alignment: FractionalOffset.center,
                                child: Container(
                                  height: 30,
                                  width: 30,
                                  child: Icon(
                                    Icons.pause_circle_filled_outlined,
                                    color: Colors.red,
                                    size: 40,
                                  ),
                                ),
                              ),
                      ),
                      onTap: () async {


                        if (_isPlay) {
                          _pause();
                         // await audio.pause();
                          setState(() {
                            _isPlay = false;
                          });
                        } else {
                          setState(() {
                            _isPlay = true;
                          });
                          onPlayAudio();
                         // await audio.play(_audioPath, isLocal: true);
                          /*audio.onPlayerCompletion.listen((event) {
                            setState(() {
                              _isPlay = false;
                            });
                          });*/
                        }
                      },
                    ),
                  ],
                ),
                start_enregistrement
                    ? Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: Center(
                          child: LinearProgressIndicator(),
                        ),
                      )
                    : Text(""),
                SizedBox(
                  height: 10,
                ),
                end_enregistrement
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          TextClasse(
                            text: "Audio enregistré avec sucess",
                            fontSize: 15,
                            family: "OsemiBold",
                            color: Colors.green,
                          ),
                          Icon(
                            Icons.beenhere_rounded,
                            color: Colors.green,
                            size: 40,
                          ),
                        ],
                      )
                    : Text(""),
                SizedBox(
                  height: 50,
                ),
                Container(
                  child: FloatingActionButton(
                    elevation: 10.0,
                    child: Image.asset(
                      'lib/images/sendMessage.png',
                      width: 40,
                      height: 40,
                    ),
                    onPressed: () => sendDifficultyIn()),
                ),
                SizedBox(
                  height: 200,
                )
              ],
            ),
          ),
        ),
      ),
      /*floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,

      floatingActionButton: FloatingActionButton(
          elevation: 10.0,
          child: Image.asset(
            'lib/images/sendMessage.png',
            width: 40,
            height: 40,
          ),
          onPressed: () => sendDifficultyIn()),*/
    );
  }


  _init() async {
    try {
      bool hasPermission = await FlutterAudioRecorder2.hasPermissions ?? false;

      if (hasPermission) {
        String customPath = '/audio_recorder_';
        io.Directory appDocDirectory;
//        io.Directory appDocDirectory = await getApplicationDocumentsDirectory();
        if (io.Platform.isIOS) {
          appDocDirectory = await getApplicationDocumentsDirectory();
        } else {
          appDocDirectory = (await getExternalStorageDirectory());
        }

        // can add extension like ".mp4" ".wav" ".m4a" ".aac"
        customPath = appDocDirectory.path +
            customPath +
            DateTime.now().millisecondsSinceEpoch.toString();

        // .wav <---> AudioFormat.WAV
        // .mp4 .m4a .aac <---> AudioFormat.AAC
        // AudioFormat is optional, if given value, will overwrite path extension when there is conflicts.
        _recorder =
            FlutterAudioRecorder2(customPath, audioFormat: AudioFormat.WAV);

        await _recorder.initialized;
        // after initialization
        var current = await _recorder.current(channel: 0);
        print(current);
        // should be "Initialized", if all working fine
        setState(() {
          _current = current;
          _currentStatus = current.status;
          print(_currentStatus);
        });
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: new Text("You must accept permissions")));
      }
    } catch (e) {
      print(e);
    }
  }

  void onPlayAudio() async {
    AudioPlayer audioPlayer = AudioPlayer();
    await audioPlayer.play(_current.path, isLocal: true);
    audioPlayer.onPlayerCompletion.listen((event) {
      setState(() {
        _isPlay = false;
      });
    });
  }


  _resume() async {
    await _recorder.resume();
    setState(() {});
  }

  _pause() async {
    await _recorder.pause();
    var isRecording = await AudioRecorder.isRecording();
    setState(() {
      _isRecording = isRecording;
    });
    print("File path of the record: ${_current?.path}");
  }

  _start() async {
    try {
      await _recorder.start();
      var recording = await _recorder.current(channel: 0);
      var isRecording = await AudioRecorder.isRecording();
      setState(() {
        start_enregistrement = true;
        end_enregistrement = false;
        _tick = 0;
        _current = recording;
      });


      _timer = Timer.periodic(Duration(seconds: 1), (timer) async {
        var current = await _recorder.current(channel: 0);
        double db = await AudioRecorder.db;
        setState(() {
          _tick = timer.tick;
          _current = current;
          _currentStatus = _current.status;
          isRecording = isRecording;
          if (_tick == 30) _stopRecord();

        });
        //if (_tick == 30) _stopRecord();
      });
      
    } catch (e) {
      print(e);
    }
  }



  Future sendDifficultyIn() async{
    if(last_local_difficulte_id ==0)
      {
        setState(() {
          last_local_difficulte_id = 1;
        });

      }

    print("apres : $last_local_difficulte_id");
    if(_image1 == null && _current.path != "" && cvaSelect!=null){


      EasyLoading.show(status: 'Chargement', dismissOnTap: false);
      final uri=Uri.parse("http://192.168.43.99:8000/api/difficultes");
      var request =http.MultipartRequest('POST',uri);

      request.fields['path_img']="Pas d'image";
      request.fields['relais_id']=relais_id.toString();
      request.fields['cva_id']=cvaSelect.toString();
      request.fields['local_id']=last_local_difficulte_id.toString();

      var audio = await http.MultipartFile.fromPath( "audio", _current.path );
      request.files.add(audio);
      var response = await request.send();


      UserCtr().insertDifficulte(Difficulte(
          path_img: "Pas d'image",
          path_audio: _current.path,
          path_response: "Pas de reponse",
          relais_id: relais_id,
          cva_id: cvaSelect,
          date_:formattedDateTime()
      ));


      if(response.statusCode==200){

        EasyLoading.dismiss();
        EasyLoading.showSuccess("Envoie Réussie");
        EasyLoading.dismiss();
        setState(() {

          start_enregistrement = false;
          end_enregistrement = false;
          _tick = 0;

        });

      }else{
        EasyLoading.dismiss();
        displaySnackBarNom(context,
            "Veuillez vérifier votre connexion internet", Colors.white);

      }
    }else if(_image1 != null && _current.path != "" && cvaSelect!=null){
      EasyLoading.show(status: 'Chargement', dismissOnTap: false);
      final uri=Uri.parse("http://192.168.43.99:8000/api/difficultes");
      var request =http.MultipartRequest('POST',uri);

      request.fields['relais_id']=relais_id.toString();
      request.fields['cva_id']=cvaSelect.toString();
      request.fields['local_id']=last_local_difficulte_id.toString();

      var audio = await http.MultipartFile.fromPath( "audio", _current.path );
      request.files.add(audio);

      var image = await http.MultipartFile.fromPath( "image", _image1.path );
      request.files.add(image);

      var response = await request.send();

      UserCtr().insertDifficulte(Difficulte(
          path_img: _image1.path,
          path_audio: _current.path,
          path_response: "Pas de reponse",
          relais_id: relais_id,
          cva_id: cvaSelect,
          date_:formattedDateTime()
      ));

      if(response.statusCode==200){

        EasyLoading.dismiss();
        EasyLoading.showSuccess("Envoie Réussie");
        EasyLoading.dismiss();

        setState(() {

          start_enregistrement = false;
          end_enregistrement = false;
          _tick = 0;
          _image1 =null;

        });

        /*Navigator.push(
            context, MaterialPageRoute(builder: (context) => HomeScreen()));*/

      }else{
        EasyLoading.dismiss();
        displaySnackBarNom(context,
            "Veuillez vérifier votre connexion internet", Colors.white);
      }
    }
    else {
      displaySnackBarNom(context,
          "Vous devez enregistré un audio et choisir une cva avant de continuer", Colors.white);
    }

  }

  Future<void> sendDifficultyInDb() async {


    if (_image1 == null && _current.path != "" && cvaSelect!=null) {
      EasyLoading.show(status: 'Chargement', dismissOnTap: false);
      File file = widget.localFileSystem.file(_audioPath);
      List<int> fileBytes = await file.readAsBytes();
      String base64audio = base64Encode(fileBytes);
      userRepo
          .sendDifficuty(Difficulte(
              path_img: "Pas d'image",
              path_audio: base64audio,
              relais_id: relais_id,
              cva_id: cvaSelect))
          .then((value) {
        if (value == 201) {
          EasyLoading.dismiss();
          EasyLoading.showSuccess("Envoie Réussie");
          EasyLoading.dismiss();
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => HomeScreen()));
        }
      });
    } else if (_image1 != null && _current.path != "" && cvaSelect!=null) {
      EasyLoading.show(status: 'Chargement', dismissOnTap: false);
      final bytes = Io.File(_image1.path).readAsBytesSync();
      String img64 = base64Encode(bytes);
      File file = widget.localFileSystem.file(_audioPath);
      List<int> fileBytes = await file.readAsBytes();
      String base64audio = base64Encode(fileBytes);

      userRepo
          .sendDifficuty(Difficulte(
              path_img: img64,
              path_audio: base64audio,
              relais_id: relais_id,
              cva_id: cvaSelect))
          .then((value) {
        if (value == 201) {
          EasyLoading.dismiss();
          EasyLoading.showSuccess("Envoie Réussie");
          EasyLoading.dismiss();
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => HomeScreen()));
        }
      });
    } else {
      displaySnackBarNom(context,
          "Vous devez enregistré un audio et choisir une cva avant de continuer", Colors.white);
    }
  }

  Widget _displayChild1() {
    if (_image1 == null) {
      return Padding(
          padding: const EdgeInsets.fromLTRB(14, 70, 14, 70),
          child: !gallery
              ? Column(
                  children: [
                    Container(
                      height: 100,
                      width: 200,
                      child: Image.asset(PathImagesAudios.GALLERY),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextClasse(
                      text: "Ajouter une photo de la gallery",
                      family: "OSemiBold",
                    ),
                  ],
                )
              : Column(
                  children: [
                    Container(
                      height: 100,
                      width: 200,
                      child: Image.asset(PathImagesAudios.CAMERA),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextClasse(
                      text: "Prendre une photo",
                      family: "OSemiBold",
                    ),
                  ],
                ));
    } else {
      return Container(
        height: 200,
        child: Image.file(
          _image1,
          fit: BoxFit.cover,
          width: double.infinity,
        ),
      );
    }
  }

  void _selectImage(Future<File> pickImage, int imageNumber) async {
    File tempImg = await pickImage;
    switch (imageNumber) {
      case 1:
        setState(() => _image1 = tempImg);
        break;
    }
  }

  displaySnackBarNom(BuildContext context, String text, Color couleur) {
    final snackBar = SnackBar(
      content: Text(text, style: TextStyle(color: couleur, fontSize: 13)),
      duration: Duration(seconds: 2),
    );
    // ignore: deprecated_member_use
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  Future<void> _showSelectionDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                GestureDetector(
                  child: TextClasse(
                    text: "GALLERY",
                    family: "OSemiBold",
                  ),
                  onTap: () {
                    getImageFromGallery();
                  },
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                Divider(
                  height: 20,
                  color: Colors.black,
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                GestureDetector(
                  child: TextClasse(
                    text: "CAMERA",
                    family: "OSemiBold",
                  ),
                  onTap: () {
                    getImageFromCamera();
                  },
                ),
              ],
            ),
          ));
        });
  }

  Future getImageFromGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    setState(() {
      _image1 = widget.localFileSystem.file(pickedFile.path);
    });
    // _saveFile(_image);
    Navigator.of(context).pop();
  }

  Future getImageFromCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    setState(() {
      _image1 = widget.localFileSystem.file(pickedFile.path);
    });
    //_saveFile(_image);
    Navigator.of(context).pop();
  }

  void dispose() {
    if (this.mounted) audio.stop();
    super.dispose();
  }
}
