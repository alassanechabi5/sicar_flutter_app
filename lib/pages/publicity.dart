import 'dart:io';

/*import 'package:audioplayers/audio_cache.dart';*/
import 'package:audioplayers/audioplayers.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:sicar/components/constants_path.dart';
import 'package:sicar/components/text_classe.dart';
import 'package:sicar/database/controller_db.dart';
import 'package:sicar/models/checkInternet.dart';
import 'package:sicar/models/partenaire.dart';
import 'package:url_launcher/url_launcher.dart';

import '../global_config.dart';


class Publicity extends StatefulWidget {
  @override
  _PublicityState createState() => _PublicityState();
}

class _PublicityState extends State<Publicity> {

  List<Partenaire> partenaires = [];


  AudioCache cache = AudioCache();
  AudioPlayer player;
  bool autoPlayCaroussel2=true;
  List<bool> playCaroussel = [];
  List<String> audios =  [];
  List<String> images =  [];
  List<String> imagesDisplay =  [];


  List<String> audiosMenus = [];

  Future<void> _downloadAndCacheAudio() async {
    try {
      final cacheManager = DefaultCacheManager();

      FileInfo fileInfo;


      audios.forEach((element) async {
        print( Configuration.base_img_video_url  + element);
        fileInfo = await cacheManager.getFileFromCache(Configuration.base_img_video_url + element);
        if(fileInfo?.file == null){
          print("il veut le télécharger");
          fileInfo = await cacheManager.downloadFile(Configuration.base_img_video_url + element);
        }
        print(fileInfo?.file.path);
        final file = File(fileInfo?.file.path);
        setState(() {
          audiosMenus.add(fileInfo?.file.path);
          playCaroussel.add(false);
        });
      });



    } catch (e) {
      throw (e);
    }
  }

  Future<void> _downloadAndCacheImage() async {
    try {
      final cacheManager = DefaultCacheManager();

      FileInfo fileInfo;

      images.forEach((element) async{
        print(Configuration.base_img_video_url+element);

        fileInfo = await cacheManager.getFileFromCache(Configuration.base_img_video_url+element);
        if(fileInfo?.file == null){
          print("il veut le télécharger");
          fileInfo = await cacheManager.downloadFile(Configuration.base_img_video_url+element);
        }
        print(fileInfo?.file.path);

        setState(() {
          imagesDisplay.add(fileInfo?.file.path);
        });


      });

      /*for(int i=0; i<images.length; i++){

        fileInfo = await cacheManager.getFileFromCache(Configuration.base_img_video_url+images[i]);
        if(fileInfo?.file == null){
          print("il veut le télécharger");
          fileInfo = await cacheManager.downloadFile(Configuration.base_img_video_url+images[i]);
        }
        print(fileInfo?.file.path);

        setState(() {
          imagesDisplay[i]=fileInfo?.file.path;
        });

      }*/


    } catch (e) {
      throw (e);
    }
  }

  List<Image> imagesModules(){
    List<Image> imgs = [];
    imagesDisplay.forEach((element) {
      setState(() {
        imgs.add(Image.file(File(element), fit: BoxFit.cover,));
      });
    });
    return imgs;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    playCaroussel.forEach((element) {
      if(element==true)
        player.stop();
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    UserCtr().getPartenaire().then((value) {
      print(value);
      value.forEach((element) {
        setState(() {
          partenaires.add(Partenaire.fromJson(element));
          audios.add(element["path_audio"]);
          images.add(element["path_img"]);
        });
      });
      _downloadAndCacheAudio();
      _downloadAndCacheImage();

    });

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  TextClasse(text: "Nouvelles", fontSize: 20, family: "OsemiBold", textAlign: TextAlign.center, color: Colors.white,),
      ),
      body: (partenaires.length!=0 && audiosMenus.length == partenaires.length && imagesDisplay.length == partenaires.length) ?SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 200,),
              carousselProjetsPartenaires()
            ],
          ),
        ),
      ):Center(child: CircularProgressIndicator(),)
    );
  }

  Widget imageCaroussel2(String pathImage, int i){
    return   Container(
      width: double.infinity,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(pathImage),
              fit: BoxFit.cover
          )
      ),
      child: Stack(
        children: [
          Align(
            child:Container(
              decoration: BoxDecoration(
                color:Colors.blue,
                shape: BoxShape.circle,
              ),
              child:Center(
                child: Icon((!playCaroussel[i])?Icons.play_arrow:Icons.pause, color: Colors.white, size: 25,),
              ),
              height: 50,
            ),
          )
        ],
      ),
    );
  }
  Widget carousselProjetsPartenaires() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 200,
      color: Theme.of(context).primaryColor,
      child: Carousel(
        boxFit: BoxFit.cover,
        autoplay: true,
        autoplayDuration: Duration(seconds: 6),
        animationCurve: Curves.linearToEaseOut,
        animationDuration: Duration(seconds: 2),
        dotSize: 10.0,
        dotIncreasedColor: Color(0xFFEF7532),
        dotPosition: DotPosition.bottomCenter,
        dotVerticalPadding: 10.0,
        indicatorBgPadding: 7.0,
        dotBgColor: Colors.red.withOpacity(0),
        onImageTap:(i) async {

          _seeMoreButtonCaroussel2(i);
          print(i);
        } ,
        images: imagesModules()
      ),
    );
  }

  _seeMoreButtonCaroussel2(int i) async {
    if(playCaroussel[i]){
      setState(() {
        playCaroussel[i]=false;
      });
      player.pause();
    } else {
      playCaroussel.forEach((element) {
        if(element==true)
          player.stop();
      });
      setState(() {
        playCaroussel[i]=true;
      });
      player = await cache.play(audiosMenus[i]);
      player.onPlayerCompletion.listen((event) {
        print("Laudio est fini");
        print(playCaroussel[i]);
        setState(() {
          playCaroussel[i]=false;
        });
        print(playCaroussel[i]);
      });
    }
  }

}
