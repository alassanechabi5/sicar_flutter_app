import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:sicar/components/constants_path.dart';
import 'package:sicar/components/hexadecimal.dart';
import 'package:sicar/components/text_classe.dart';


class Evaluation extends StatefulWidget {
  List<String> images = [];

  Evaluation({this.images});
  @override
  _EvaluationState createState() => _EvaluationState();
}

class _EvaluationState extends State<Evaluation> {

  bool status = false;

  List<int> entier = [];
  List<int> resultIndex = [];
  List<String> result = [];
  var rng = new Random();
  List<int> melange;

  List<String> imagesMelanges ;
  int numberTap = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.images.forEach((element) {
      print(element);
    });
    print(widget.images.length);
    imagesMelanges = widget.images;

    imagesMelanges.shuffle();

    widget.images.forEach((element) {
      entier.add(0);
      result.add("");
      resultIndex.add(0);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HexColor("#03224c"),
      appBar: AppBar(
          title:  TextClasse(text: "Evaluation", fontSize: 20, family: "OsemiBold", color: Colors.white,),
          actions: [
            IconButton(icon: Icon(Icons.refresh, color: Colors.white, size: 40,), onPressed: (){
              setState(() {
                result = [];
                entier = [];
                resultIndex = [];
              });
              widget.images.forEach((element) {
                setState(() {
                  numberTap = 0;
                  entier.add(0);
                  resultIndex.add(0);
                });
              });

            }),
            SizedBox(width: 10,)
          ],

      ),
      body: ( entier.length==widget.images.length && imagesMelanges.length!=0)?ListView(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child:  StaggeredGridView.countBuilder(
                crossAxisCount: 4,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: imagesMelanges.length,
                itemBuilder: (BuildContext context, int index) => (entier[index]==0)? InkWell(
                  onTap:(){
                    setState(() {
                      entier[index] = 1;
                      numberTap = numberTap+1;
                      print(numberTap);
                      result[index] = imagesMelanges[index];
                      resultIndex[index]=numberTap;
                    });
                  },
                  child: Image.file(File(imagesMelanges[index]), fit: BoxFit.cover, height: 250,),
                ):Container(
                    color: Color(0xFFEF7532),
                    child: Column(
                      children: [
                         CircleAvatar(
                          backgroundColor: Colors.white,
                          child: new Text('${resultIndex[index]}'),
                        ),
                        Image.file(File(imagesMelanges[index]), fit: BoxFit.cover, height: 100,),
                      ],
                    )
                ),
                staggeredTileBuilder: (int index) =>
                new StaggeredTile.count(2, 2),
                mainAxisSpacing: 20.0,
                crossAxisSpacing: 20.0,
              ),
          ),
          SizedBox(height: 20,),
          SizedBox(width: 50,),
          Center(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 40),
              height: 50,
              child: FlatButton(
                  color: Colors.green,
                  onPressed: (){

              }, child: Icon(Icons.send, color: Colors.white, size: 40,)),
            ),
          ),
          SizedBox(height: 20,),
        ],
      ):Center(child: CircularProgressIndicator(),)
    );
  }

  void checkResult(){
   if(result == []){
     print("Veuillez choisir les images dans l'ordre");

   } else {

         if(result == widget.images){
           print("Le resultat est bon");
         }

         else
           print("Le resultat n'est pas juste");
         }
   }
}
