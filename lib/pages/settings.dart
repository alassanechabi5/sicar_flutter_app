import 'package:flutter/material.dart';
import 'package:sicar/components/submit_button.dart';
import 'package:sicar/components/text_classe.dart';
import 'package:sicar/database/controller_db.dart';
import 'package:sicar/models/relais.dart';
import 'package:sicar/user_repository.dart' as userRepo;


class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  Relais relais;

  String code = "+229";
  String pays = "Bénin";
  String logo = "";
  TextEditingController _name = TextEditingController();
  TextEditingController _lastName = TextEditingController();
  TextEditingController _commune = TextEditingController();
  TextEditingController _ethnie = TextEditingController();
  TextEditingController _village = TextEditingController();

  bool _enableName = false;
  bool _enableLastName = false;
  bool _enableCommune = false;
  bool _enableEthnie = false;
  bool _enableVillage = false;
  bool _enableModify = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userRepo.getCurrentUser().then((value) {
      print(value);
      setState(() {
        relais = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            centerTitle: true,
            title: TextClasse(
              text: "Paramètres",
              fontSize: 20,
              family: "OsemiBold",
              textAlign: TextAlign.center,
              color: Colors.white,
            )),
        body: (relais != null)
            ? Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: ListView(
                  children: [
                    SizedBox(
                      height: 50,
                    ),
                    TextFormField(
                      style:  TextStyle(
                          fontFamily: "OSemiBold",
                          fontSize: 15,
                          color: Colors.black),
                      controller: _name,
                      enabled: _enableName,
                      decoration: InputDecoration(
                          hintText: relais.last_name,
                          hintStyle: TextStyle(
                              fontFamily: "OSemiBold",
                              fontSize: 15,
                              color: Colors.black)),
                      // ignore: missing_return
                      validator: (value) {
                        // ignore: missing_return
                        if (value.isEmpty)
                          return ("Veuillez entrer le nom de l'entreprise");
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      style:  TextStyle(
                          fontFamily: "OSemiBold",
                          fontSize: 15,
                          color: Colors.black),
                      controller: _lastName,
                      enabled: _enableLastName,
                      decoration: InputDecoration(
                          hintText: relais.first_name,
                          hintStyle: TextStyle(
                              fontFamily: "OSemiBold",
                              fontSize: 15,
                              color: Colors.black)),
                      // ignore: missing_return
                      validator: (value) {
                        // ignore: missing_return
                        if (value.isEmpty)
                          return ("Veuillez entrer le nom de l'entreprise");
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      style:  TextStyle(
                          fontFamily: "OSemiBold",
                          fontSize: 15,
                          color: Colors.black),
                      controller: _commune,
                      enabled: _enableCommune,
                      decoration: InputDecoration(
                          hintText: relais.commune,
                          hintStyle: TextStyle(
                              fontFamily: "OSemiBold",
                              fontSize: 15,
                              color: Colors.black)),
                      // ignore: missing_return
                      validator: (value) {
                        // ignore: missing_return
                        if (value.isEmpty)
                          return ("Veuillez entrer le nom de l'entreprise");
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      style:  TextStyle(
                          fontFamily: "OSemiBold",
                          fontSize: 15,
                          color: Colors.black),
                      controller: _ethnie,
                      enabled: _enableEthnie,
                      decoration: InputDecoration(
                          hintText: relais.ethnic_group,
                          hintStyle: TextStyle(
                              fontFamily: "OSemiBold",
                              fontSize: 15,
                              color: Colors.black)),
                      // ignore: missing_return
                      validator: (value) {
                        // ignore: missing_return
                        if (value.isEmpty)
                          return ("Veuillez entrer le nom de l'entreprise");
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      style:  TextStyle(
                          fontFamily: "OSemiBold",
                          fontSize: 15,
                          color: Colors.black),
                      controller: _village,
                      enabled: _enableVillage,
                      decoration: InputDecoration(
                          hintText: relais.village,
                          hintStyle: TextStyle(
                              fontFamily: "OSemiBold",
                              fontSize: 15,
                              color: Colors.black)),
                      // ignore: missing_return
                      validator: (value) {
                        // ignore: missing_return
                        if (value.isEmpty)
                          return ("Veuillez entrer le nom de l'entreprise");
                      },
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    (_enableModify)?submitButtonWithText(context, "Enregistrer", (){
                       setState(() {
                         _enableModify = false;
                          _enableName = false;
                          _enableLastName = false;
                          _enableCommune = false;
                          _enableEthnie = false;
                          _enableVillage = false;
                          _enableModify = false;
                       });
                    }):submitButtonWithText(context, "Modifier", (){
                       setState(() {
                         _enableModify = true;
                         _enableName = true;
                         _enableLastName = true;
                         _enableCommune = true;
                         _enableEthnie = true;
                         _enableVillage = true;
                         _enableModify = true;
                       });
                    })
                    /*Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextClasse(text: "Nom: ",  fontSize: 20, family: "OsemiBold", textAlign: TextAlign.center, ),
         TextFormField(
                    controller: _name,
                    enabled: _enableName,
                    decoration: InputDecoration(
                        hintText: relais.first_name,
                        hintStyle: TextStyle(fontFamily: "OSemiBold", fontSize: 15, color: Colors.black)
                    ),
                    // ignore: missing_return
                    validator: (value){
                      // ignore: missing_return
                      if(value.isEmpty)
                        return ("Veuillez entrer le nom de l'entreprise");
                    },
                  ),              ],
            ),
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                TextClasse(text: "Prénoms: ",  fontSize: 20, family: "OsemiBold", textAlign: TextAlign.center, ),
                TextClasse(text: relais.last_name,  fontSize: 20, family: "ORegular", textAlign: TextAlign.center, maxLines: 2,),
              ],
            ),
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                TextClasse(text: "Numéro: ",  fontSize: 20, family: "OsemiBold", textAlign: TextAlign.center, ),
                TextClasse(text: relais.phone,  fontSize: 20, family: "ORegular", textAlign: TextAlign.center, maxLines: 2,),
              ],
            ),
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                TextClasse(text: "Commune: ",  fontSize: 20, family: "OsemiBold", textAlign: TextAlign.center, ),
                TextClasse(text: relais.commune,  fontSize: 20, family: "ORegular", textAlign: TextAlign.center, maxLines: 2,),
              ],
            ),
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                TextClasse(text: "Ethnie: ",  fontSize: 20, family: "OsemiBold", textAlign: TextAlign.center, ),
                TextClasse(text: relais.ethnic_group,  fontSize: 20, family: "ORegular", textAlign: TextAlign.center, maxLines: 2,),
              ],
            ),

            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                TextClasse(text: "Village: ",  fontSize: 20, family: "OsemiBold", textAlign: TextAlign.center, ),
                TextClasse(text: relais.village,  fontSize: 20, family: "ORegular", textAlign: TextAlign.center, maxLines: 2,),
              ],
            ),*/
                  ],
                ),
              )
            : Center(
                child: CircularProgressIndicator(),
              ));
  }
}
