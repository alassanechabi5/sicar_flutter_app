import 'dart:async';
import 'package:connectivity/connectivity.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sicar/database/controller_db.dart';
import 'package:sicar/models/evaluation_network.dart';
import 'package:sicar/user_repository.dart' as userRepo;



class CheckInternet {
  StreamSubscription<DataConnectionStatus> listener;
  var InternetStatus = "Unknown";
  var contentmessage = "Unknown";
  List<EvaluationNetwork> evaluationList = [];
  bool isEmpty;

  Future<void> sendEvaluationNetwork() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    print( sharedPreferences.getInt("language"));
    int idLangue = sharedPreferences.getInt("language");
    UserCtr().getEvaluationNetwork().then((value) {
      print("Ici");
      print(value.isNotEmpty);
      if(value.isNotEmpty)
         value.forEach((element) {
           element.langage_id = idLangue;
           print("La liste des evaluations");
           print(element);
           userRepo.sendEvaluation(element).then((value) {
             if(value)
               UserCtr().deleteEvaluationNetwork(element.id);
           });
         });
    });
  }

  checkConnection(BuildContext context) async{
    listener = DataConnectionChecker().onStatusChange.listen((status) {
      switch (status){
        case DataConnectionStatus.connected:
          InternetStatus = "Connected to the Internet";
          contentmessage = "Connected to the Internet";
          print("Il y a la connexion");
          sendEvaluationNetwork();
          break;
        case DataConnectionStatus.disconnected:
          InternetStatus = "You are disconnected to the Internet. ";
          contentmessage = "Please check your internet connection";
          break;
      }
    });
    return await DataConnectionChecker().connectionStatus;
  }

  // New methode to check internet connection

   Future <bool> isInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      if (await DataConnectionChecker().hasConnection) {
        print("Mobile data detected & internet connection confirmed.");
        return true;
      }
      else {
        print("No internet connection ");
        return false;
      }
    } else if (connectivityResult == ConnectivityResult.wifi) {
      if (await DataConnectionChecker().hasConnection) {
        print("wifi data detected & internet connection confirmed");
        return true;
      } else {
        print("No internet connection");
        return false;
      }
    } else {
      print(
          "Neither mobile data or WIFI detected, not internet  connection found ");
      return false;
    }
  }

  void _showDialog(String title,String content ,BuildContext context){
    showDialog(
        context: context,
        builder: (BuildContext context){
          return AlertDialog(
              title: new Text("Veuillez vérifier votre connexion internet"),
              content: new Text(content),
              actions: <Widget>[
                new FlatButton(
                    onPressed: (){
                      Navigator.of(context).pop();
                    },
                    child:new Text("Fermer") )
              ]
          );
        }
    );

  }
}