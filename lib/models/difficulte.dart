class Difficulte {
  int id;
  int relais_id;
  int cva_id;
  String path_img = "Par defaut";
  String path_audio = "Par defaut";
  String path_response = "Par defaut";
  String date_;
  int difficulte_id;
  String date_reponse = "Par defaut";


  Difficulte({this.id,this.relais_id, this.path_img, this.path_audio,this.path_response, this.cva_id,this.date_,this.date_reponse});

  factory Difficulte.fromJson(Map<String, dynamic> json) {
    return Difficulte(
      id: json['id'],
      relais_id: json['relais_id'],
      path_img: json['path_img'],
      path_audio: json['path_audio'],
      path_response: json['path_response'],
      cva_id: json['cva_id'],
      date_: json['date_'],
      date_reponse: json['date_reponse'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['relais_id'] = this.relais_id;
    data['path_img'] = this.path_img;
    data['path_audio'] = this.path_audio;
    data['path_response'] = this.path_response;
    data['cva_id'] = this.cva_id;
    data['date_'] = this.date_;
    data['date_reponse'] = this.date_reponse;
    return data;
  }

}