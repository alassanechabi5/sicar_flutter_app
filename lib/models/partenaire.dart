class Partenaire {
  int id ;
  String intitule;
  String zone_intervention;
  String path_img;
  String path_audio ;
  String description ;
  String slug ;


  Partenaire({this.intitule,this.zone_intervention, this.id, this.path_img, this.path_audio, this.description, this.slug,});



  factory Partenaire.fromJson(Map<String, dynamic> json) {
    return Partenaire(
      id: json['id'],
      intitule: json['intitule'],
      zone_intervention: json['zone_intervention'],
      path_img: json['path_img'],
      path_audio: json['path_audio'],
      description: json['description'],
      slug: json['slug'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['intitule'] = this.intitule;
    data['zone_intervention'] = this.zone_intervention;
    data['id'] = this.id;
    data['path_img'] = this.path_img;
    data['path_audio'] = this.path_audio;
    data['description'] = this.description;
    data['slug'] = this.slug;

    return data;
  }

}