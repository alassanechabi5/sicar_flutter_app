class ReponseDifficulte {

  int user_id;
  int difficulte_id;
  int local_difficulte_id;
  int relais_id;
  int id ;
  String path_audio = "Par defaut";
  String slug;
  String description;
  String date_reponse;

  ReponseDifficulte({this.user_id, this.difficulte_id,this.local_difficulte_id, this.relais_id, this.id, this.path_audio, this.slug, this.description,this.date_reponse});



  factory ReponseDifficulte.fromJson(Map<String, dynamic> json) {
    return ReponseDifficulte(
      user_id: json['user_id'],
      difficulte_id: json['difficulte_id'],
      local_difficulte_id: json['local_difficulte_id'],
      relais_id: json['relais_id'],
      id: json['id'],
      path_audio: json['path_audio'],
      slug: json['slug'],
      description: json['description'],
      date_reponse: json['date_reponse'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.user_id;
    data['difficulte_id'] = this.difficulte_id;
    data['local_difficulte_id'] = this.local_difficulte_id;
    data['relais_id'] = this.relais_id;
    data['id'] = this.id;
    data['path_audio'] = this.path_audio;
    data['slug'] = this.slug;
    data['description'] = this.description;
    data['date_reponse'] = this.date_reponse;
    return data;
  }

}