class Cvas {
  int id;
  String intitule ;
  String path_img ;
  String description ;


  Cvas({this.id, this.path_img,  this.description, this.intitule,});



  factory Cvas.fromJson(Map<String, dynamic> json) {
    return Cvas(
      id: json['id'],
      path_img: json['path_img'],
      description: json['description'],
      intitule: json['intitule'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['path_img'] = this.path_img;
    data['description'] = this.description;
    data['intitule'] = this.intitule;

    return data;
  }

}