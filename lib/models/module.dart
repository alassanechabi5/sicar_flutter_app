class Module {
  int id;
  int cva_id;
  int language_id;
  String num_chapitre ;
  String intitule ;
  String path_img ;
  String path_audio ;
  String path_video ;
  String description ;
 // String img_chapitre;


  Module({this.cva_id, this.num_chapitre, this.path_img, this.path_audio, this.path_video, this.description, this.intitule, this.language_id, this.id, /*this.img_chapitre*/});



  factory Module.fromJson(Map<String, dynamic> json) {
    return Module(
      id: json['id'],
      cva_id: json['cva_id'],
      language_id: json['language_id'],
      num_chapitre: json['num_chapitre'],
      path_img: json['path_img'],
      path_audio: json['path_audio'],
      path_video: json['path_video'],
      description: json['description'],
      intitule: json['intitule'],
      //img_chapitre: json['img_chapitre'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['cva_id'] = this.cva_id;
    data['language_id'] = this.language_id;
    data['num_chapitre'] = this.num_chapitre;
    data['path_img'] = this.path_img;
    data['path_audio'] = this.path_audio;
    data['path_video'] = this.path_video;
    data['description'] = this.description;
    data['intitule'] = this.intitule;
    //data['img_chapitre'] = this.img_chapitre;

    return data;
  }

}