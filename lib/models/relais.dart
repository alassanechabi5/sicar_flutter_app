class Relais {
  int relais_id;
  int id;
  String last_name = "Par defaut";
  String first_name = "Par defaut";
  String ethnic_group = "Par defaut";
  String sexe = "Par defaut";
  String commune = "Par defaut";
  String village = "Par defaut";
  String phone;
  String whatsapp = "00000000";
  String language;
  int partenaire_id;
  String avatar;
  String slug;
  String activite_principale;
  String activite_secondaire;
  String full_name;


  Relais({this.last_name, this.first_name, this.ethnic_group, this.sexe, this.commune, this.village, this.phone, this.whatsapp, this.language, this.relais_id, this.partenaire_id, this.id, this.full_name,this.activite_principale,this.activite_secondaire});



  factory Relais.fromJson(Map<String, dynamic> json) {
    return Relais(
      relais_id: json['relais_id'],
      last_name: json['last_name'],
      first_name: json['first_name'],
      ethnic_group: json['ethnic_group'],
      sexe: json['sexe'],
      commune: json['commune'],
      village: json['village'],
      phone: json['phone'],
      whatsapp: json['whatsapp'],
      language: json['language'],
      id: json['id'],
      partenaire_id: json['partenaire_id'],
      full_name: json['full_name'],
      activite_principale: json['activite_principale'],
      activite_secondaire: json['activite_secondaire'],

    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['relais_id'] = this.relais_id;
    data['last_name'] = this.last_name;
    data['first_name'] = this.first_name;
    data['ethnic_group'] = this.ethnic_group;
    data['sexe'] = this.sexe;
    data['commune'] = this.commune;
    data['village'] = this.village;
    data['phone'] = this.phone;
    data['whatsapp'] = this.whatsapp;
    data['language'] = this.language;
    data['id'] = this.id;
    data['full_name'] = this.full_name;
    data['partenaire_id'] = this.partenaire_id;
    data['activite_principale'] = this.activite_principale;
    data['activite_secondaire'] = this.activite_secondaire;

    return data;
  }

}