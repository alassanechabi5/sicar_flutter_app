class CvaSuivi {
  int id;
  int user_id;
  String date;
  int cva_id;
  String module;




  CvaSuivi({this.user_id, this.date, this.cva_id, this.module, this.id,});
  factory CvaSuivi.fromJson(Map<String, dynamic> json) {
    return CvaSuivi(
      id: json["id"],
      user_id: json['user_id'],
      date: json['date'],
      cva_id: json['cva_id'],
      module: json['module'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.user_id;
    data['date'] = this.date;
    data['cva_id'] = this.cva_id;
    data['module'] = this.module;
    return data;
  }

}