class Evaluation {
  int id;
  int user_id;
  String date;
  int cva_id;
  String note_evaluation;
  String cva_name;





  Evaluation({this.user_id, this.date, this.cva_id, this.note_evaluation, this.id, this.cva_name});



  factory Evaluation.fromJson(Map<String, dynamic> json) {
    return Evaluation(
      id: json["id"],
      user_id: json['user_id'],
      date: json['date'],
      cva_id: json['cva_id'],
      note_evaluation: json['note_evaluation'],
      cva_name: json['cva_name'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.user_id;
    data['date'] = this.date;
    data['cva_id'] = this.cva_id;
    data['note_evaluation'] = this.note_evaluation;
    data['cva_name'] = this.cva_name;
    return data;
  }

}