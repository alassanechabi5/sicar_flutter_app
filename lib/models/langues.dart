class Langues {
  int id;
  String intitule ;
  String path_audio ;


  Langues({this.id, this.intitule, this.path_audio, });



  factory Langues.fromJson(Map<String, dynamic> json) {
    return Langues(
      id: json['id'],
      intitule: json['intitule'],
      path_audio: json['path_audio'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['intitule'] = this.intitule;
    data['path_audio'] = this.path_audio;
    return data;
  }

}