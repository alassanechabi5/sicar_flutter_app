class EvaluationNetwork {
  int id;
  int relais_id;
  int module_id;
  String note_evaluation;
  int langage_id;





  EvaluationNetwork({this.relais_id, this.module_id, this.note_evaluation, this.id, this.langage_id});



  factory EvaluationNetwork.fromJson(Map<String, dynamic> json) {
    return EvaluationNetwork(
      id: json["id"],
      relais_id: json['relais_id'],
      module_id: json['module_id'],
      note_evaluation: json['note_evaluation'],
      langage_id: json['langage_id'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['relais_id'] = this.relais_id;
    data['module_id'] = this.module_id;
    data['note_evaluation'] = this.note_evaluation;
    data['langage_id'] = this.langage_id;
    return data;
  }

}