import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;

  static Database _db;

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await create();
    return _db;
  }

  DatabaseHelper.internal();

  Future create() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String databaseDirectory = join(directory.path, "database.db");
    var bdd =
    await openDatabase(databaseDirectory, version: 1, onCreate: _onCreate);
    return bdd;
  }


  Future _onCreate(Database db, int version) async {

    await db.execute('''
    CREATE TABLE Partenaire (
    id INTEGER PRIMARY KEY, 
    intitule TEXT NULL,
    description TEXT ,
    zone_intervention TEXT ,
    path_img TEXT,
    path_audio TEXT,
    slug TEXT
    )
    ''');


    await db.execute('''
    CREATE TABLE Relais (
    id INTEGER PRIMARY KEY, 
    relais_id INTEGER,
    last_name TEXT NULL,
    first_name TEXT ,
    ethnic_group TEXT ,
    sexe TEXT,
    commune TEXT,
    village TEXT,
    phone TEXT NON NULL,
    whatsapp TEXT,
    language TEXT,
    partenaire_id INTEGER,
    avatar TEXT,
    slug TEXT,
    activite_principale TEXT,
    activite_secondaire TEXT,
    full_name TEXT
    )
    ''');


    await db.execute('''
    CREATE TABLE User (
    id INTEGER PRIMARY KEY, 
    relais_id INTEGER,
    last_name TEXT NON NULL,
    first_name TEXT ,
    ethnic_group TEXT ,
    sexe TEXT,
    commune TEXT,
    village TEXT,
    phone TEXT NON NULL,
    whatsapp TEXT,
    language TEXT,
    partenaire_id TEXT,
    avatar TEXT,
    slug TEXT,
    activite_principale TEXT,
    activite_secondaire TEXT,
    full_name TEXT
    )
    ''');

    await db.execute('''
    CREATE TABLE Langues (
    id INTEGER PRIMARY KEY, 
    intitule TEXT,
    path_audio TEXT 
    )
    ''');

    await db.execute('''
    CREATE TABLE Cvas (
    id INTEGER PRIMARY KEY, 
    intitule TEXT,
    path_img TEXT,
    description TEXT 
    )
    ''');

  await db.execute('''
    CREATE TABLE Evaluation (
    id INTEGER PRIMARY KEY, 
    user_id INTEGER,
    date TEXT,
    cva_name TEXT,
    cva_id INTEGER ,
    note_evaluation TEXT
    )
    ''');

  await db.execute('''
    CREATE TABLE EvaluationNetwork (
    id INTEGER PRIMARY KEY, 
    relais_id INTEGER,
    module_id INTEGER,
    langage_id INTEGER ,
    note_evaluation TEXT
    )
    ''');


  await db.execute('''
    CREATE TABLE CvaSuivi (
    id INTEGER PRIMARY KEY, 
    user_id INTEGER,
    date TEXT,
    cva_id INTEGER ,
    module String
    )
    ''');

    await db.execute('''
    CREATE TABLE Modules (
    id INTEGER PRIMARY KEY, 
    cva_id INTEGER,
    language_id INTEGER,
    num_chapitre TEXT,
    intitule TEXT, 
    path_img TEXT, 
    path_audio TEXT, 
    path_video TEXT, 
    description TEXT
    img_chapitre TEXT
    )
    ''');

    await db.execute('''
    CREATE TABLE ReponseDifficulte (
    id INTEGER PRIMARY KEY, 
    user_id INTEGER,
    difficulte_id INTEGER,
    local_difficulte_id INTEGER,
    relais_id INTEGER,
    description TEXT, 
    path_audio TEXT, 
    slug TEXT,
    date_reponse
    )
    ''');

    await db.execute('''
    CREATE TABLE Difficulte (
    id INTEGER PRIMARY KEY, 
    difficulte_id INTEGER,
    relais_id INTEGER,
    cva_id INTEGER,
    path_img TEXT,
    path_audio TEXT,
    path_response TEXT,
    date_ TEXT,
    date_reponse
    
    
    )
    ''');
  }

}
