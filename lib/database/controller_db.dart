import 'dart:async';
import 'dart:async';
import 'dart:async';

import 'package:sicar/models/cva_suivi.dart';
import 'package:sicar/models/cvas.dart';
import 'package:sicar/models/evaluation.dart';
import 'package:sicar/models/evaluation_network.dart';
import 'package:sicar/models/langues.dart';
import 'package:sicar/models/module.dart';
import 'package:sicar/models/partenaire.dart';
import 'package:sicar/models/relais.dart';
import 'package:sicar/models/response_difficulte.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sicar/models/difficulte.dart';

import 'database.dart';



class UserCtr {
  DatabaseHelper con = new DatabaseHelper();


  // Partenaire controller

  Future<Partenaire> insertPartenaire(Partenaire partenaire) async {

    var db = await con.db;
    partenaire.id = await db.insert("Partenaire", partenaire.toJson());
    return partenaire;
  }

  Future<List<Map<String, dynamic>>>  getPartenaire() async {
    var dbPartenaire = await con.db;
    var result = dbPartenaire.rawQuery("SELECT * FROM Partenaire");
    return result;
  }


  Future<int> updatePartenaire(Partenaire partenaire) async{
    var db = await con.db;
    return await db.rawUpdate("UPDATE Partenaire SET path_audio = ${partenaire.path_audio} WHERE id= ${partenaire.id}");
  }

  Future<int> deletePartenaire(int id) async {
    var db = await con.db;
    int res = await db.rawDelete('DELETE FROM Partenaire WHERE id = ?', [id]);
    return res;
  }

  // Difficulte controller

  Future<Difficulte> insertDifficulte(Difficulte difficulte) async {

    var db = await con.db;
    difficulte.id = await db.insert("Difficulte", difficulte.toJson());
    return difficulte;
  }

  Future<List<Difficulte>>  getDifficulte() async {
    var db = await con.db;
    List<Map<String, dynamic>>  result = await db.rawQuery("SELECT * FROM Difficulte");
    List<Difficulte> module =[];
    result.forEach((element) {
      module.add(Difficulte.fromJson(element));
    });
    return module;
  }

  Future<List<Difficulte>>  getLastDifficulte() async {
    var db = await con.db;
    List<Map<String, dynamic>>  result = await db.rawQuery("SELECT * FROM Difficulte ORDER BY id DESC LIMIT 1");
    List<Difficulte> module =[];
    result.forEach((element) {
      module.add(Difficulte.fromJson(element));
    });
    return module;
  }

  Future<int> deleteDifficulte(int id) async {
    var db = await con.db;
    int res = await db.rawDelete('DELETE FROM Difficulte WHERE id = ?', [id]);
    return res;
  }

  Future<int> updateDifficulte(String path_reponse,int id,String date_reponse) async{
    var db = await con.db;
    return await db.rawUpdate("UPDATE Difficulte SET path_response = ?,date_reponse = ? WHERE id= ?",[path_reponse,date_reponse,id]);
  }

  // Relais controller

  Future<Relais> insertRelais(Relais relais) async {
    var db = await con.db;
    relais.relais_id = await db.insert("Relais", relais.toJson());
    return relais;
  }

  Future<List<Relais>> getRelais() async{
    var db = await con.db;
    List<Map<String, dynamic>>  result = await db.rawQuery("SELECT * FROM Relais");
    List<Relais> relais = [];
    result.forEach((element) {
      relais.add( Relais.fromJson(element));
    });
    return relais;
  }

  Future<int> deleteRelais(int id) async {
    var db = await con.db;
    int res = await db.rawDelete('   DELETE FROM Relais WHERE id = ?', [id]);
    return res;
  }


  // User controller

  Future<Relais> insertUser(Relais user) async {

    var db = await con.db;
    user.relais_id = await db.insert("User", user.toJson());
    return user;
  }


  Future<Relais> getUser() async{
    var db = await con.db;
    List<Map<String, dynamic>>  result = await db.rawQuery("SELECT * FROM User LIMIT 1");
    Relais relais;
    print(result);
    result.forEach((element) {
       relais = Relais.fromJson(element);
    });
    return relais;
  }

  Future<int> deleteUser(int id) async {
    var db = await con.db;
    int res = await db.rawDelete('DELETE FROM User WHERE id = ?', [id]);
    return res;
  }


  //.
  // Langue controller

  Future<Langues> insertLangue(Langues langue) async {
    var db = await con.db;
    langue.id = await db.insert("Langues", langue.toJson());
    return langue;
  }

  Future<List<Langues>> getLangue() async{
    var db = await con.db;
    List<Map<String, dynamic>>  result = await db.rawQuery("SELECT * FROM Langues");
    List<Langues> langues = [];
    result.forEach((element) {
      langues.add( Langues.fromJson(element));
    });
    return langues;
  }

  Future<int> deleteLangue(int id) async {
    var db = await con.db;
    int res = await db.rawDelete('DELETE FROM Langues WHERE id = ?', [id]);
    return res;
  }



  // Cvas controller

  Future<Cvas> insertCvas(Cvas cvas) async {
    var db = await con.db;
    cvas.id = await db.insert("Cvas", cvas.toJson());
    return cvas;
  }


  Future<List<Cvas>> getCvas() async{
    var db = await con.db;
    List<Map<String, dynamic>>  result = await db.rawQuery("SELECT * FROM Cvas");
    List<Cvas> cvas = [];
    result.forEach((element) {
      cvas.add( Cvas.fromJson(element));
    });
    return cvas;
  }

  Future<int> deleteCva(int id) async {
    var db = await con.db;
    int res = await db.rawDelete('DELETE FROM Cvas WHERE id = ?', [id]);
    return res;
  }


  // Insert Modules

  Future<Module> insertModule(Module mod) async {
    var db = await con.db;
    bool existAlready = false;
    List<Map<String, dynamic>>  result = await db.rawQuery("SELECT * FROM Modules");
    Module module;
    result.forEach((element) {
      if(element["id"] == mod.id){
        print("Le module existe déjà");
        existAlready = true;
        deleteModule(element["id"]).whenComplete(() {
          db.insert("Modules", mod.toJson());
        });
      } else
        existAlready = false;
    });
    if(existAlready==false)
    await db.insert("Modules", mod.toJson());
    return module;
  }



  Future<List<Module>> getModule(int cva_id, int langue_id) async {
    var db = await con.db;
    List<Map<String, dynamic>>  result = await db.rawQuery("SELECT * FROM Modules");
     List<Module> module =[];
    result.forEach((element) {
      //module.add(Module.fromJson(element));
      //print(element["num_chapitre"]);
     if(element["cva_id"] == cva_id && element["language_id"] == langue_id){
       module.add(Module.fromJson(element));
     }
    });
    return module;
  }


  Future<Module> getModuleWithChapiter(int cva_id, int langue_id, String chapter) async {
    var db = await con.db;
    List<Map<String, dynamic>>  result = await db.rawQuery("SELECT * FROM Modules");
     Module module;
    result.forEach((element) {
      print(element["intitule"]);
     if(element["cva_id"] == cva_id && element["language_id"] == langue_id && element["num_chapitre"] == chapter){
       print("Il l'a trouvé");
       module  = Module.fromJson(element);
     }
    });
    return module;
  }

  /*Future<Module> getModule(int cva_id, int langue_id) async {
    var db = await con.db;
    List<Map<String, dynamic>>  result = await db.rawQuery("SELECT * FROM Modules");
     Module module;
    result.forEach((element) {
      print(element["intitule"]);
     if(element["cva_id"] == cva_id.toString() && element["language_id"] == langue_id.toString()){
       module  = Module.fromJson(element);
     }
    });
    return module;
  }*/

  Future<int> deleteModule(int id) async {
    print("Delete Module");
    var db = await con.db;
    int res = await db.rawDelete('DELETE FROM Modules WHERE id = ?', [id]).whenComplete(() {
      print("La suppresssion a marché");
    });
    return res;
  }

  Future<int> deleteTableModule() async {
    var db = await con.db;
    int res = await db.delete("Modules");
    return res;
  }

  // EVALUATION  controller


  Future<Evaluation> insertEvaluation(Evaluation evaluation) async {

    var db = await con.db;
    evaluation.id = await db.insert("Evaluation", evaluation.toJson());
    return evaluation;
  }

  Future<List<Evaluation>>  getEvaluation() async {
    var dbPartenaire = await con.db;
    List<Map<String, dynamic>>  result = await dbPartenaire.rawQuery("SELECT * FROM Evaluation ORDER BY date DESC");
    List<Evaluation> evaluation = [];
    result.forEach((element) {
      evaluation.add( Evaluation.fromJson(element));
    });
    return evaluation;
  }


  Future<int> deleteEvaluation(int id) async {
    var db = await con.db;
    int res = await db.rawDelete('DELETE FROM Evaluation WHERE id = ?', [id]);
    return res;
  }

  // EVALUATIONNETWORK  controller


  Future<EvaluationNetwork> insertEvaluationNetwork(EvaluationNetwork evaluation) async {

    var db = await con.db;
    evaluation.id = await db.insert("EvaluationNetwork", evaluation.toJson());
    return evaluation;
  }

  Future<List<EvaluationNetwork>>  getEvaluationNetwork() async {
    var dbPartenaire = await con.db;
    List<Map<String, dynamic>>  result = await dbPartenaire.rawQuery("SELECT * FROM EvaluationNetwork ");
    List<EvaluationNetwork> evaluation = [];
    result.forEach((element) {
      evaluation.add( EvaluationNetwork.fromJson(element));
    });
    return evaluation;
  }


  Future<int> deleteEvaluationNetwork(int id) async {
    var db = await con.db;
    int res = await db.rawDelete('DELETE FROM EvaluationNetwork WHERE id = ?', [id]);
    return res;
  }


  // CvaSuivi  controller


  Future<CvaSuivi> insertCvaSuivi(CvaSuivi cvaSuivi) async {
    var db = await con.db;
    cvaSuivi.id = await db.insert("CvaSuivi", cvaSuivi.toJson());
    return cvaSuivi;
  }

  Future<List<CvaSuivi>>  getCvaSuivi() async {
    var dbPartenaire = await con.db;
    List<Map<String, dynamic>>  result = await  dbPartenaire.rawQuery("SELECT * FROM CvaSuivi");
    List<CvaSuivi> cvaSuivi = [];
    result.forEach((element) {
      cvaSuivi.add( CvaSuivi.fromJson(element));
    });
   // var result = dbPartenaire.rawQuery("SELECT * FROM CvaSuivi");
    return cvaSuivi;
  }


  Future<int> deleteCvaSuivi(int id) async {
    var db = await con.db;
    int res = await db.rawDelete('DELETE FROM CvaSuivi WHERE id = ?', [id]);
    return res;
  }



  // Difficultees

  Future<ReponseDifficulte> insertReponseDifficulte(ReponseDifficulte mod) async {
    var db = await con.db;
    bool existAlready = false;
    List<Map<String, dynamic>>  result = await db.rawQuery("SELECT * FROM ReponseDifficulte");
    ReponseDifficulte module;
    result.forEach((element) {
      if(element["id"] == mod.id){
        print("La difficulte existe déjà");
        existAlready = true;
        deleteModule(element["id"]).whenComplete(() {
          db.insert("ReponseDifficulte", mod.toJson());
        });
      } else
        existAlready = false;
    });
    if(existAlready==false)
      await db.insert("ReponseDifficulte", mod.toJson());
    return module;
  }


  Future<List<ReponseDifficulte>> getReponseDifficulte() async {
    var db = await con.db;
    List<Map<String, dynamic>>  result = await db.rawQuery("SELECT * FROM ReponseDifficulte");
    List<ReponseDifficulte> module =[];
    result.forEach((element) {
        module.add(ReponseDifficulte.fromJson(element));
    });
    return module;
  }


  Future<int> deleteReponseDifficulte(int id) async {
    print("Delete difficulte");
    var db = await con.db;
    int res = await db.rawDelete('DELETE FROM ReponseDifficulte WHERE id = ?', [id]).whenComplete(() {
      print("La suppresssion a marché");
    });
    return res;
  }

}