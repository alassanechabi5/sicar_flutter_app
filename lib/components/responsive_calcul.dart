import 'package:flutter/material.dart';


double largeurPerCent(double size, BuildContext context) {
  double width=MediaQuery.of(context).size.width;
  double largeur;
  largeur = (size/411.4)*width;
  return largeur;
}


double longueurPerCent(double size, BuildContext context) {
  double height=MediaQuery.of(context).size.height;
  double longueur;
  longueur = (size/683.4)*height;
  return longueur;
}