class PathImagesAudios{


  // Constant url images cva
  static const String LOGO_SICA = "lib/images/logosica.jpeg";

  static const String APICULTURE = "lib/images/apiculture.jpg";

  static const String BAOBAB = "lib/images/baobab.jpg";

  static const String KARITE = "lib/images/karite.jpg";

  static const String MUNGBEAN = "lib/images/mungBean.jpg";

  static const String SESAME = "lib/images/sesame.jpg";

  static const String SODJA = "lib/images/soja.jpg";


  // Constants url images menu
  static const String ELEARNING = "lib/images/eLearning.png";

  static const String CHAT = "lib/images/chat.png";

  static const String RELAY = "lib/images/relay.png";



  // Constants url images fournisseurs
  static const String ABREUVOIRS = "lib/images/abreuvoirs.jpg";

  static const String NEEMBIO = "lib/images/neemBio.png";

  static const String PULVERISATEUR16LITRES = "lib/images/pulverisateur16Litres.jpg";

  static const String PEPINIEREDEPIMENT = "lib/images/pépinièreDePiment.jpg";

  static const String SEMENCESMARAICHERES = "lib/images/semencesMaraîchères.jpg";


  // Constants url images projets
  static const String RBT_WAP_IMAGE = "lib/images/rbt_wap.png";

  static const String ONG_ATPF_IMAGE = "lib/images/ong_atpf.jpg";

  static const String ONG_EVD_IMAGE = "lib/images/evd.jpg";

  static const String PAASTIGNAMB_OMG_IMAGE = "lib/images/paastignamb.jpg";

  static const String HELVETAS = "lib/images/helvetas.jpg";

  static const String ENABEL = "lib/images/enabel.png";



  // Constants url vers les sites projets
  static const String  RBT_WAP_LINK= "https://web.facebook.com/complexeWAP";

  static const String  ONG_ATPF_LINK= "https://web.facebook.com/ONG-ATPF-113376306757739";

  static const String  ONG_EVD_LINK= "https://web.facebook.com/ong.evd";

  static const String  PAASTIGNAMB_OMG_LINK= "https://www.paastignamb.org/";




  // Constants url images composants
  static const String PLAY = "lib/images/play.png";

  static const String MICROPHONE = "lib/images/microphone.png";

  static const String CAMERA = "lib/images/camera.png";

  static const String GALLERY = "lib/images/gallery.png";

  static const String SEND = "lib/images/send.png";

  static const String PAUSE = "lib/images/pause.png";

  static const String NOMICROPHONE = "lib/images/noMicrophone.png";




  // Constant url audios
  static const String ALL_LANGUES = "lib/audios/all_langues.mp3";

  static const String FORMATIONFRANCAIS="lib/audios/audio_french_d_2_4.mp3";

  static const String FRANCAISAUDIO="french.wav";

  static const String BIALIAUDIO="lib/audios/biali.mp3";

  static const String BARIBAAUDIO="lib/audios/bariba.mp3";




  // Constant numéro fournisseurs intrants
  static const String NUMEROABREUVOIRS = "+22997898574";

  static const String NUMERONEEMBIO = "+22921383236";

  static const String NUMEROPULVERISATEUR16LITRES = "+22997898574";

  static const String NUMEROPEPINIEREDEPIMENT = "+22996619072";
  
  static const String NUMEROSEMENCESMARAICHERES = "+22966071308";


  //Constants  audio langues noms
  static const String ZERMA = "ZERMA";

  static const String MORE = "MORE";

  static const String HAOUSSA = "HAOUSSA";

  static const String GOURMENTCHE = "GOURMENTCHE";

  static const String FRENCH = "FRENCH";

  static const String FONGBE = "FONGBE";

  static const String ENGLISH = "ENGLISH";


  static const String BIALI = "BIALI";



  //Constants CVA noms
  static const String APICULTURECVA = "Apiculture";

  static const String BAOBABCVA = "BAOBAB";

  static const String KARITECVA = "KARITE";

  static const String MUNGBEANCVA = "MUNGBEAN";

  static const String SESAMECVA = "SESAME";

  static const String SOJACVA = "SOJA";



  // Constant audios menus Français

  static const String FR1 = "français1.mp3";
  static const String FR2 = "français2.mp3";
  static const String FR3 = "français3.mp3";

  // Constant audios menus Biali

  static const String BI1 = "biali1.mp3";
  static const String BI2 = "biali2.mp3";
  static const String BI3 = "biali3.mp3";

  // Constant audios menus Bariba

  static const String BA1 = "bariba1.mp3";
  static const String BA2 = "bariba2.mp3";
  static const String BA3 = "bariba3.mp3";

}