


import 'package:flutter/material.dart';
import 'package:sicar/components/text_classe.dart';
import 'hexadecimal.dart';




InkWell submitButton(BuildContext context, String text, Function onTap, ){
  return InkWell(
    onTap: onTap,
    child: Container(
      height: 50,
      width: 50,
      child: CircleAvatar(
        backgroundColor: HexColor("#119600"),
        child: Icon(Icons.arrow_forward, color: Colors.white, size: 50,),
      ),
    )
  );
}


InkWell submitButtonWithText(BuildContext context, String text, Function onTap, ){
  return InkWell(
    onTap: onTap,
    child: Container(
      color: Theme.of(context).primaryColor,
      height: 40,
      width: 200,
      child: Center(child: TextClasse(text: text, color: Colors.white, family: "OsemiBold", fontSize: 20,))
    )
  );
}
